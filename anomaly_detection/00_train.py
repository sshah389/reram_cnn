import os
import glob
import sys
import numpy

from tqdm import tqdm

import common as com
import pytorch_model

import pandas as pd

from torch.utils.data import Dataset, DataLoader
import torch
import os
from torch import nn

param = com.yaml_load()

def list_to_vector_array(file_list,
                         msg="calc...",
                         n_mels=64,
                         frames=5,
                         n_fft=1024,
                         hop_length=512,
                         power=2.0):
    """
    convert the file_list to a vector array.
    file_to_vector_array() is iterated, and the output vector array is concatenated.

    file_list : list [ str ]
        .wav filename list of dataset
    msg : str ( default = "calc..." )
        description for tqdm.
        this parameter will be input into "desc" param at tqdm.

    return : numpy.array( numpy.array( float ) )
        vector array for training (this function is not used for test.)
        * dataset.shape = (number of feature vectors, dimensions of feature vectors)
    """
    # calculate the number of dimensions
    dims = n_mels * frames

    # iterate file_to_vector_array()
    for idx in tqdm(range(len(file_list)), desc=msg):
        vector_array = com.file_to_vector_array(file_list[idx],
                                                n_mels=n_mels,
                                                frames=frames,
                                                n_fft=n_fft,
                                                hop_length=hop_length,
                                                power=power)
        if idx == 0:
            dataset = numpy.zeros((vector_array.shape[0] * len(file_list), dims), float)
        dataset[vector_array.shape[0] * idx: vector_array.shape[0] * (idx + 1), :] = vector_array

    return dataset


def file_list_generator(target_dir,
                        dir_name="train",
                        ext="wav"):
    """
    target_dir : str
        base directory path of the dev_data or eval_data
    dir_name : str (default="train")
        directory name containing training data
    ext : str (default="wav")
        file extension of audio files

    return :
        train_files : list [ str ]
            file list for training
    """
    com.logger.info("target_dir : {}".format(target_dir))

    # generate training list
    training_list_path = os.path.abspath("{dir}/{dir_name}/*.{ext}".format(dir=target_dir, dir_name=dir_name, ext=ext))
    files = sorted(glob.glob(training_list_path))
    if len(files) == 0:
        com.logger.exception("no_wav_file!!")

    com.logger.info("train_file num : {num}".format(num=len(files)))
    return files

if __name__ == "__main__":
    # check mode
    # "development": mode == True
    # "evaluation": mode == False
    mode = com.command_line_chk()
    if mode is None:
        sys.exit(-1)
        
    # make output directory
    os.makedirs(param["model_directory"], exist_ok=True)

    # load base_directory list
    dirs = com.select_dirs(param=param, mode=mode)

    # loop of the base directory
    for idx, target_dir in enumerate(dirs):
        print("[{idx}/{total}] {dirname}".format(dirname=target_dir, idx=idx+1, total=len(dirs)))

        # set path
        machine_type = os.path.split(target_dir)[1]
        model_file_path = "{model}/model_{machine_type}.pth".format(model=param["model_directory"],
                                                                     machine_type=machine_type)
        history_img = "{model}/history_{machine_type}.png".format(model=param["model_directory"],
                                                                  machine_type=machine_type)

        if os.path.exists(model_file_path):
            com.logger.info("model exists")
            continue

        # generate dataset
        files = file_list_generator(target_dir)
        train_data = list_to_vector_array(files,
                                          msg="generate train_dataset",
                                          n_mels=param["feature"]["n_mels"],
                                          frames=param["feature"]["frames"],
                                          n_fft=param["feature"]["n_fft"],
                                          hop_length=param["feature"]["hop_length"],
                                          power=param["feature"]["power"])
        
        class Pytorch_Dataset(Dataset):
            def __init__(self, data):
                self.data = torch.from_numpy(data.astype('float32'))

            def __len__(self):
                return len(self.data)

            def __getitem__(self, idx):
                return self.data[idx]
            
        print("============== Converting to pytorch dataset ==============")
        py_dataset= Pytorch_Dataset(train_data)
        train_dataloader = DataLoader(py_dataset, batch_size=param["fit"]["batch_size"], shuffle=param["fit"]["shuffle"])

        print("============== MODEL TRAINING ==============")

        model = pytorch_model.get_model(param["feature"]["n_mels"] * param["feature"]["frames"])

        #print(f"Model structure: {model}\n\n")


        learning_rate = 1e-3
        batch_size = 64
        epochs = 5

        def train_loop(dataloader, model, loss_fn, optimizer):
            size = len(dataloader)
            model.train()
            for batch, X in enumerate(dataloader):   
                reconstruction = model(X)   
                loss = loss_fn(reconstruction, X)
                loss.backward()   
                optimizer.step()    
                optimizer.zero_grad()   

                if batch % 100 == 0:    
                    loss = loss.item()
                    print(f"loss: {loss:>7f}  [{(100*batch/size):>3f}%]")


        def test_loop(dataloader, model, loss_fn):
            model.eval()
            num_batches = len(dataloader)
            test_loss = 0
            with torch.no_grad():
                for X in dataloader:
                    reconstruction = model(X)
                    test_loss += loss_fn(reconstruction, X).item()
            test_loss /= num_batches
            print(f"Avg loss: {test_loss:>8f} \n")

        loss_fn = nn.MSELoss()

        optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate)   # Choose the optimizer, TODO from param?

        epochs = param["fit"]["epochs"]
        for t in range(epochs):
            print(f"Epoch {t+1}\n-------------------------------")
            train_loop(train_dataloader, model, loss_fn, optimizer)
        print("Done")
        
        torch.save(model.state_dict(), model_file_path)
        com.logger.info("save_model -> {}".format(model_file_path))
        print("============== END TRAINING ==============")
