
########################################################################
# imports
from torch import nn # NNs building blocks
import torch
########################################################################

class NeuralNetwork(nn.Module):   #create subclass of nn.Module
    def __init__(self, input_dim):
        super().__init__()    #super to get all the attributes from parent's init ; methods are automaticall inherited since it is a child class
        self.inputDim = input_dim
        self.encoder = nn.Sequential(
            nn.Linear(input_dim, 128),
            nn.BatchNorm1d(128),
            nn.ReLU(),
            nn.Linear(128, 128),
            nn.BatchNorm1d(128),
            nn.ReLU(),
            nn.Linear(128, 128),
            nn.BatchNorm1d(128),
            nn.ReLU(),
            nn.Linear(128, 128),
            nn.BatchNorm1d(128),
            nn.ReLU(),
            nn.Linear(128, 8),
            nn.BatchNorm1d(8),
            nn.ReLU()
        )
        self.decoder = nn.Sequential(
            nn.Linear(8, 128),
            nn.BatchNorm1d(128),
            nn.ReLU(),
            nn.Linear(128, 128),
            nn.BatchNorm1d(128),
            nn.ReLU(),
            nn.Linear(128, 128),
            nn.BatchNorm1d(128),
            nn.ReLU(),
            nn.Linear(128, 128),
            nn.BatchNorm1d(128),
            nn.ReLU(),
            nn.Linear(128, input_dim)
        )

    def forward(self, x):
        x = self.encoder(x)
        x = self.decoder(x)
        return x
    

########################################################################
# keras model
########################################################################
def get_model(inputDim):
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    model = NeuralNetwork(inputDim).to(device)
    return model
#########################################################################

