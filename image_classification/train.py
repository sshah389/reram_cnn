import numpy as np
import matplotlib.pyplot as plt
import pickle
from torchvision import transforms
import torch
import pytorch_model
from torch.utils.data import Dataset, DataLoader
import torch.optim as optim
import torch.nn as nn
import sys

import datetime

EPOCHS = 35
BS = 32

# get date ant time to save model
dt = datetime.datetime.today()
year = dt.year
month = dt.month
day = dt.day
hour = dt.hour
minute = dt.minute

class CIFAR10Dataset(Dataset): 
    def __init__(self, data, labels, transform=None):
        self.data = data
        self.labels = labels
        self.transform = transform
    
    def __len__(self):
        return len(self.data)
    
    def __getitem__(self, idx):
        image = self.data[idx]
        label = self.labels[idx]
        
        if self.transform:
            image = self.transform(image)
        
        return image, label


def unpickle(file):
    
    with open(file, 'rb') as fo:
        data = pickle.load(fo, encoding='bytes')
    return data

def to_categorical(y, num_classes=None):
    y = torch.tensor(y, dtype=torch.int64)
    if num_classes is None:
        num_classes = int(torch.max(y)) + 1
    return torch.nn.functional.one_hot(y, num_classes=num_classes)

def load_cifar_10_data(data_dir, negatives=False):

    meta_data_dict = unpickle(data_dir + "/batches.meta")
    cifar_label_names = meta_data_dict[b'label_names']
    cifar_label_names = np.array(cifar_label_names)

    # training data
    cifar_train_data = None
    cifar_train_filenames = []
    cifar_train_labels = []

    for i in range(1, 6):
        cifar_train_data_dict = unpickle(data_dir + "/data_batch_{}".format(i))
        if i == 1:
            cifar_train_data = cifar_train_data_dict[b'data']
        else:
            cifar_train_data = np.vstack((cifar_train_data, cifar_train_data_dict[b'data']))
        cifar_train_filenames += cifar_train_data_dict[b'filenames']
        cifar_train_labels += cifar_train_data_dict[b'labels']

    cifar_train_data = cifar_train_data.reshape((len(cifar_train_data), 3, 32, 32))
    if negatives:
        cifar_train_data = cifar_train_data.transpose(0, 2, 3, 1).astype(np.float32)
    else:
        cifar_train_data = np.rollaxis(cifar_train_data, 1, 4)
    cifar_train_filenames = np.array(cifar_train_filenames)
    cifar_train_labels = np.array(cifar_train_labels)

    cifar_test_data_dict = unpickle(data_dir + "/test_batch")
    cifar_test_data = cifar_test_data_dict[b'data']
    cifar_test_filenames = cifar_test_data_dict[b'filenames']
    cifar_test_labels = cifar_test_data_dict[b'labels']

    cifar_test_data = cifar_test_data.reshape((len(cifar_test_data), 3, 32, 32))
    if negatives:
        cifar_test_data = cifar_test_data.transpose(0, 2, 3, 1).astype(np.float32)
    else:
        cifar_test_data = np.rollaxis(cifar_test_data, 1, 4)
    cifar_test_filenames = np.array(cifar_test_filenames)
    cifar_test_labels = np.array(cifar_test_labels)

    return cifar_train_data, cifar_train_filenames, to_categorical(cifar_train_labels), \
        cifar_test_data, cifar_test_filenames, to_categorical(cifar_test_labels), cifar_label_names


if __name__ == "__main__":
    
    cifar_10_dir = 'cifar-10-batches-py'

    train_data, train_filenames, train_labels, test_data, test_filenames, test_labels, label_names = \
        load_cifar_10_data(cifar_10_dir)

    print("Train data: ", train_data.shape)
    print("Train filenames: ", train_filenames.shape)
    print("Train labels: ", train_labels.shape)
    print("Test data: ", test_data.shape)
    print("Test filenames: ", test_filenames.shape)
    print("Test labels: ", test_labels.shape)
    print("Label names: ", label_names)



    # #display some random training images in a 25x25 grid
    # num_plot = 5
    # f, ax = plt.subplots(num_plot, num_plot)
    # for m in range(num_plot):
    #     for n in range(num_plot):
    #         idx = np.random.randint(0, train_data.shape[0])
    #         ax[m, n].imshow(train_data[idx])
    #         ax[m, n].get_xaxis().set_visible(False)
    #         ax[m, n].get_yaxis().set_visible(False)
    # f.subplots_adjust(hspace=0.1)
    # f.subplots_adjust(wspace=0)
    # plt.show()




    # Define transformations
    train_transform = transforms.Compose([
        transforms.ToPILImage(),  
        # transforms.RandomHorizontalFlip(),
        # transforms.RandomRotation(15),
        # transforms.RandomAffine(0, translate=(0.1, 0.1)),
        transforms.ToTensor(),
    ])    

    train_dataset = CIFAR10Dataset(train_data, train_labels, transform=train_transform)
    train_loader = DataLoader(train_dataset, batch_size=BS, shuffle=True, num_workers=2)
    test_dataset = CIFAR10Dataset(test_data, test_labels, transform=train_transform)
    test_loader = DataLoader(test_dataset, batch_size=BS, shuffle=True, num_workers=2)

    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    # Initialize model and move it to the device
    model = pytorch_model.resnet_v1_eembc().to(device)

    optimizer = optim.Adam(model.parameters(), lr=0.001, momentum=0.9)
    loss_fn = nn.CrossEntropyLoss()

    # print(f"Model structure: {model}\n\n")
                        
    model.train()
    loss_data  = []
    for epoch in range(EPOCHS):
        running_loss = 0.0
        for batch, (images, labels) in enumerate(train_loader): 
            batch = batch +1 
            images, labels = images.to(device), labels.to(device)      
            
            optimizer.zero_grad()
            outputs = model(images)
            labels = torch.argmax(labels, dim=1)
            
            loss = loss_fn(outputs, labels)
            loss.backward()
            optimizer.step()

            running_loss += loss.item()

            if batch%500 == 0:
                print("Epoch", epoch+1, "batch", batch, "\nLoss: ", running_loss)
                loss_data.append(running_loss)
                running_loss = 0
                model.eval()
                total = 0
                correct = 0
                for batch_t, (images_t, labels_t) in enumerate(test_loader):  
                    images_t, labels_t = images_t.to(device), labels_t.to(device)
                    outputs_t= torch.softmax(model(images_t), dim = 1)
                    outputs_t = torch.argmax(outputs_t, dim = 1)
                    labels_t = torch.argmax(labels_t, dim=1)

                    
                    total += labels_t.size(0)
                    correct += (outputs_t == labels_t).sum().item()
                print(f'Accuracy: {correct/total}\n')

                model.train()      
         


        # print(f'Epoch {epoch + 1}, Loss: {running_loss / len(train_loader)}')
    plt.figure(figsize=(10, 5))
    plt.plot(loss_data, label='Loss')
    plt.title('Training Loss')
    plt.xlabel('Every 100th batch')
    plt.ylabel('Loss')
    plt.legend()
    plt.show()


    model_name = "trainedResnet5.pth"
    path = "trained_models/" + model_name
    torch.save(model.state_dict(), path)
