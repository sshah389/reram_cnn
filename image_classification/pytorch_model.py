import torch.nn as nn
import torch
import torch.nn.functional as F
import torch.nn.init as init


class ResNet(nn.Module):
    def __init__(self, num_classes=10, num_channels=3):
        super(ResNet, self).__init__()

        self.relu = nn.ReLU(inplace=True)
        filter_size = 16

        self.conv1 = nn.Conv2d(num_channels, filter_size, kernel_size=3, stride=1, padding='same', bias=False)
        self.batch_norm1 = nn.BatchNorm2d(filter_size)

        #First stack
        self.conv2 = nn.Conv2d(filter_size, filter_size, kernel_size=3, stride=1, padding='same', bias=False)
        self.batch_norm2 = nn.BatchNorm2d(filter_size)
        self.conv3 = nn.Conv2d(filter_size, filter_size, kernel_size=3, stride=1, padding='same', bias=False)
        self.batch_norm3 = nn.BatchNorm2d(filter_size)
        
        filter_size = filter_size*2
        #Second Stack
        self.conv4 = nn.Conv2d(filter_size//2, filter_size, kernel_size=3, stride=2, padding=2, bias=False)
        self.batch_norm4 = nn.BatchNorm2d(filter_size)
        self.conv5 = nn.Conv2d(filter_size, filter_size, kernel_size=3, stride=1, padding='same', bias=False)
        self.batch_norm5 = nn.BatchNorm2d(filter_size)
        self.convaux4 = nn.Conv2d(filter_size//2, filter_size, kernel_size=3, stride=2, padding=2, bias=False)


        filter_size = filter_size*2
        #Third Stack
        self.conv6 = nn.Conv2d(filter_size//2, filter_size, kernel_size=3, stride=2, padding=2, bias=False)
        self.batch_norm6 = nn.BatchNorm2d(filter_size)
        self.conv7 = nn.Conv2d(filter_size, filter_size, kernel_size=3, stride=1, padding='same', bias=False)
        self.batch_norm7 = nn.BatchNorm2d(filter_size)
        self.convaux6 = nn.Conv2d(filter_size//2, filter_size, kernel_size=3, stride=2, padding=2, bias=False)

        


        self.avgpool = nn.AdaptiveAvgPool2d((1, 1))
        self.fc = nn.Linear(64, num_classes)
         
    def forward(self, x):
        x = self.conv1(x)
        x = self.relu(self.batch_norm1(x))

        y = self.conv2(x)
        y = self.relu(self.batch_norm2(y))
        y = self.conv3(y)
        y = self.batch_norm3(y)
        x = self.relu(y + x)

        y = self.conv4(x)
        y = self.relu(self.batch_norm4(y))
        y = self.conv5(y)
        y = self.batch_norm5(y)
        x = self.convaux4(x)
        x = self.relu(y + x)

        y = self.conv6(x)
        y = self.relu(self.batch_norm6(y))
        y = self.conv7(y)
        y = self.batch_norm7(y)
        x = self.convaux6(x)
        x = self.relu(y + x)

        pool_size = min(x.size(2), x.size(3))

        x = nn.AvgPool2d((pool_size, pool_size))(x)  

        x = torch.flatten(x, 1)  # Flatten the tensor

        x = self.fc(x)  # Fully connected layer, replace 64 with the appropriate input features size
 
        return x

    
def resnet_v1_eembc():
   model = ResNet()
   return model
        

