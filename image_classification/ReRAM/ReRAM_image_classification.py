from ReRAM import ReRAMNet, ReRAMLinear, ReRAMConv2d, test_fit
from ReRAM import get_fil_bins, get_fil_from_bin, quant_weight, quant_dac_adc, FIL_BINS0

import torch
import torchvision
from torchvision import datasets, transforms
import torch.nn as nn
import torch.optim as optim
import matplotlib.pyplot as plt
import torch.nn.functional as F
from torch.optim.lr_scheduler import StepLR
from sklearn.metrics import confusion_matrix
import seaborn as sn
import pandas as pd

ENABLE_BIAS = True
Q_STEP = 6 
ADC_QUANT = 8
DAC_QUANT = 8
plt.rcParams['figure.figsize'] = [15, 15]
batch_size = 32

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

transform = transforms.Compose(
    [
        transforms.ToTensor(),
        transforms.Pad(2),
        transforms.Lambda(lambda x: x / 255)  # Scale the input to be between 0 and 1
    ]
)

trainset = datasets.CIFAR10(root='./data', train=True, download=True, transform=transform)
trainloader = torch.utils.data.DataLoader(trainset, batch_size=batch_size, shuffle=True, num_workers=2)



testset = datasets.CIFAR10(root='./data', train=False, download=True, transform=transform)
testloader = torch.utils.data.DataLoader(testset, batch_size=batch_size, shuffle=False, num_workers=2)


import torch
import torch.nn as nn
import torch.nn.functional as F

class ResNet(ReRAMNet): 
    def __init__(self, num_classes=10, num_channels=3):
        super().__init__()

        self.relu = nn.ReLU(inplace=True)
        filter_size = 16

        self.conv1 = ReRAMConv2d(num_channels, filter_size, kernel_size=3, stride=1, padding='same', bias=False)
        self.batch_norm1 = nn.BatchNorm2d(filter_size)
        self._register_reram_layer_('conv1', self.conv1)

        # First stack
        self.conv2 = ReRAMConv2d(filter_size, filter_size, kernel_size=3, stride=1, padding='same', bias=False)
        self.batch_norm2 = nn.BatchNorm2d(filter_size)
        self._register_reram_layer_('conv2', self.conv2)

        self.conv3 = ReRAMConv2d(filter_size, filter_size, kernel_size=3, stride=1, padding='same', bias=False)
        self.batch_norm3 = nn.BatchNorm2d(filter_size)
        self._register_reram_layer_('conv3', self.conv3)

        filter_size = filter_size * 2
        # Second stack
        self.conv4 = ReRAMConv2d(filter_size // 2, filter_size, kernel_size=3, stride=2, padding=2, bias=False)
        self.batch_norm4 = nn.BatchNorm2d(filter_size)
        self._register_reram_layer_('conv4', self.conv4)

        self.conv5 = ReRAMConv2d(filter_size, filter_size, kernel_size=3, stride=1, padding='same', bias=False)
        self.batch_norm5 = nn.BatchNorm2d(filter_size)
        self._register_reram_layer_('conv5', self.conv5)

        self.convaux4 = ReRAMConv2d(filter_size // 2, filter_size, kernel_size=3, stride=2, padding=2, bias=False)
        self._register_reram_layer_('convaux4', self.convaux4)

        filter_size = filter_size * 2
        # Third stack
        self.conv6 = ReRAMConv2d(filter_size // 2, filter_size, kernel_size=3, stride=2, padding=2, bias=False)
        self.batch_norm6 = nn.BatchNorm2d(filter_size)
        self._register_reram_layer_('conv6', self.conv6)

        self.conv7 = ReRAMConv2d(filter_size, filter_size, kernel_size=3, stride=1, padding='same', bias=False)
        self.batch_norm7 = nn.BatchNorm2d(filter_size)
        self._register_reram_layer_('conv7', self.conv7)

        self.convaux6 = ReRAMConv2d(filter_size // 2, filter_size, kernel_size=3, stride=2, padding=2, bias=False)
        self._register_reram_layer_('convaux6', self.convaux6)

        self.avgpool = nn.AdaptiveAvgPool2d((1, 1))
        self.fc = ReRAMLinear(filter_size, num_classes, bias=ENABLE_BIAS)
        self._register_reram_layer_('fc', self.fc)

    def forward(self, x):
        x = self.input_quant_fn(x)

        
        x = self.conv1(x)
        x = self.relu(self.batch_norm1(x))
        
        if torch.any(x >= 1):
            print(f"Some values are above 1 in ")
        if torch.any(x < 0):
            print(f"Some values are below 0 in")
        
        y = self.conv2(x)
        y = self.relu(self.batch_norm2(y))
        y = self.conv3(y)
        y = self.batch_norm3(y)
        x = self.relu(y + x)

        y = self.conv4(x)
        y = self.relu(self.batch_norm4(y))
        y = self.conv5(y)
        y = self.batch_norm5(y)
        x = self.convaux4(x)
        x = self.relu(y + x)

        y = self.conv6(x)
        y = self.relu(self.batch_norm6(y))
        y = self.conv7(y)
        y = self.batch_norm7(y)
        x = self.convaux6(x)
        x = self.relu(y + x)

        pool_size = min(x.size(2), x.size(3))
        x = nn.AvgPool2d((pool_size, pool_size))(x)  

        x = torch.flatten(x, 1) 

        x = self.fc(x)  

        x = self.output_quant_fn(x)

        return x


criterion = nn.CrossEntropyLoss()
net=None
net=ResNet()

def train_model(net, trainloader, testloader, criterion, initial_lr=0.001, lr_schedule=None, weight_decay=0, num_epochs=2):
    net=net.to(device)
    net.reset_current_measurment()

    optimizer = optim.Adam(net.parameters(), lr=initial_lr, weight_decay=weight_decay)

    if lr_schedule:
        scheduler = lr_schedule(optimizer, initial_lr)

    for epoch in range(num_epochs):
        running_loss = 0.0
        total = 0
        correct = 0
        for i, data in enumerate(trainloader, 0):
            inputs, labels = data[0].to(device), data[1].to(device)

            optimizer.zero_grad()

            # Forward pass
            outputs = net(inputs)
            loss = criterion(outputs, labels)

            # Backward pass and optimization
            loss.backward()
            # sys.exit(-1)

            optimizer.step()


            running_loss += loss.item()
            if i % 20 == 19:  # print every 2000 mini-batches
                train_accuracy = 100 * correct / 100
                print('[%d, %5d] loss: %.3f ' %
                      (epoch + 1, i + 1, running_loss / 20))
                running_loss = 0.0

        if lr_schedule:
            scheduler.step()

    print('Finished Training')

def lr_schedule(optimizer, initial_lr):
    return StepLR(optimizer, step_size=5, gamma=0.1)  # Reduce learning rate by a factor of 0.1 every 5 epochs



# Train the model
train_model(net, trainloader, testloader, criterion, initial_lr=0.001, lr_schedule=lr_schedule, weight_decay=0.0001, num_epochs=1)
