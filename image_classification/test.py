import numpy as np
import matplotlib.pyplot as plt
import pickle
import tensorflow as tf
from sklearn.metrics import roc_auc_score
import pytorch_model
from torch.utils.data import Dataset, DataLoader
from torchvision import transforms
import torch.optim as optim
import torch.nn as nn
import torch
from sklearn.metrics import roc_auc_score
import train
import eval_functions_eembc
import sys

from eval_functions_eembc import calculate_accuracy, calculate_auc

# if True uses the official MLPerf Tiny subset of CIFAR10 for validation
# if False uses the full CIFAR10 validation set

PERF_SAMPLE = True
BS = 32

class CIFAR10Dataset(Dataset): 
    def __init__(self, data, labels, transform=None):
        self.data = data
        self.labels = labels
        self.transform = transform
    
    def __len__(self):
        return len(self.data)
    
    def __getitem__(self, idx):
        image = self.data[idx]
        label = self.labels[idx]
        
        if self.transform:
            image = self.transform(image)
        
        return image, label


if __name__ == "__main__":

    cifar_10_dir = 'cifar-10-batches-py'

    train_data, train_filenames, train_labels, test_data, test_filenames, test_labels, label_names = \
        train.load_cifar_10_data(cifar_10_dir)

    if PERF_SAMPLE:
        _idxs = np.load('perf_samples_idxs.npy')
        test_data = test_data[_idxs]
        test_labels = test_labels[_idxs]
        test_filenames = test_filenames[_idxs]

    print("Test data: ", test_data.shape)
    print("Test filenames: ", test_filenames.shape)
    print("Test labels: ", test_labels.shape)
    print("Label names: ", label_names.shape)
   

    test_transform = transforms.Compose([
        transforms.ToPILImage(),  
        # transforms.RandomHorizontalFlip(),
        # transforms.RandomRotation(15),
        # transforms.RandomAffine(0, translate=(0.1, 0.1)),
        transforms.ToTensor(), 
    ]) 



    test_dataset = CIFAR10Dataset(test_data, test_labels, transform=test_transform)
    test_loader = DataLoader(test_dataset, batch_size=BS, shuffle=True, num_workers=2)

    model_name = "trainedResnet35.pth"
    path = "trained_models/" + model_name

    model = pytorch_model.resnet_v1_eembc().to(torch.device("cuda" if torch.cuda.is_available() else "cpu"))
    model.load_state_dict(torch.load(path, map_location=torch.device('cpu')))
    model.eval()

    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")


    correct = 0
    total = 0
    all_predictions = []
    all_labels = []
    noargmax_all_labels = []
    with torch.no_grad():
        for data, labels in test_loader:  

            noargmax_all_labels.extend(labels.cpu().numpy())  

            outputs = model(data)

            probabilities = torch.nn.functional.softmax(outputs, dim=1).cpu().numpy()

            predicted = np.argmax(probabilities, axis=1)

            all_predictions.extend(probabilities)

            labels = np.argmax(labels.cpu().numpy(), axis = 1)

            all_labels.extend(labels)

            total += labels.shape[0]

            correct += np.sum(predicted == labels)


    all_predictions = np.array(all_predictions)
    all_labels = np.array(all_labels)
    noargmax_all_labels = np.array(noargmax_all_labels)

    
    accuracy_pytorch = correct / total
    print("Performances on cifar10 test set")
    print("PyTorch evaluation")
    print("Accuracy PyTorch: ", accuracy_pytorch)
    print("---------------------")
    
    print("EEMBC calculate_accuracy method:")
    accuracy = calculate_accuracy(all_predictions, all_labels)
    print("---------------------")

    auc_scikit = roc_auc_score(noargmax_all_labels, all_predictions)
    print("sklearn.metrics.roc_auc_score method")
    print("AUC sklearn: ", auc_scikit)
    print("---------------------")

    print("EEMBC calculate_auc method")
    auc_eembc = eval_functions_eembc.calculate_auc(all_predictions, all_labels, label_names, model_name)
    print("---------------------")
