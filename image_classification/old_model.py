import torch.nn as nn
import torch.nn.functional as F
import torch.nn.init as init

class Block(nn.Module):
    expansion = 1
    def __init__(self, in_channels, out_channels, stride=1):
        super(Block, self).__init__()

        self.conv1 = nn.Conv2d(in_channels, out_channels, kernel_size=3, padding=1, stride=stride, bias=False)
        init.kaiming_normal_(self.conv1.weight, nonlinearity='relu')
        self.batch_norm1 = nn.BatchNorm2d(out_channels)
        self.relu = nn.ReLU(inplace=True)

        self.conv2 = nn.Conv2d(out_channels, out_channels, kernel_size=3, padding=1, stride=1, bias=False)
        init.kaiming_normal_(self.conv2.weight, nonlinearity='relu')
        self.batch_norm2 = nn.BatchNorm2d(out_channels)

        self.use_identity_conv = stride != 1 or in_channels != out_channels
        if self.use_identity_conv:
            self.identity_conv = nn.Conv2d(in_channels, out_channels, kernel_size=1, padding=0, stride=stride, bias=False)
            init.kaiming_normal_(self.identity_conv.weight, nonlinearity='relu')

    def forward(self, x):
        identity = x

        out = self.conv1(x)
        out = self.relu(self.batch_norm1(out))

        out = self.conv2(out)
        out = self.batch_norm2(out)

        if self.use_identity_conv:
            identity = self.identity_conv(identity)

        out += identity
        out = self.relu(out)
        return out 

class ResNet(nn.Module):
    def __init__(self, num_classes=10, num_channels=3):
        super(ResNet, self).__init__()

        self.conv1 = nn.Conv2d(num_channels, 16, kernel_size=3, stride=1, padding='same', bias=False)
        init.kaiming_normal_(self.conv1.weight, nonlinearity='relu')
        self.batch_norm1 = nn.BatchNorm2d(16)
        self.relu = nn.ReLU()

        self.layer1 = Block(16, 16)
        self.layer2 = Block(16, 32, stride=2)
        self.layer3 = Block(32, 64, stride=2)

        self.avgpool = nn.AdaptiveAvgPool2d((1, 1))
        self.fc = nn.Linear(64, num_classes)
         
    def forward(self, x):
        x = self.conv1(x)
        x = self.relu(self.batch_norm1(x))

        x = self.layer1(x)
        x = self.layer2(x)
        x = self.layer3(x)

        x = self.avgpool(x)
        x = x.reshape(x.shape[0], -1)
        x = self.fc(x)
        
        return x

    
def resnet_v1_eembc():
   model = ResNet()
   return model
        

