# ReRAM Pytorch Model

## Requirements
- python3
- pytorch
- matplotlib
- numpy
- sklearn

## Description

`LeNetTrainTest.py` trains and tests a CNN using the MNIST dataset and specilized PyTorch layers made to simultate a Compute in Memory ReRAM array.

## Usage

```bash
python3 LeNetTrainTest.py <train|test|both|tf> [path]

# Train and test example
mkdir run1
python3 LeNetTrainTest.py both run1/r1

# Test on trained data (saved as run1/r1)
python3 LeNetTrainTest.py test run1/r1
```

# Pytorch Benchmarks

[This video](https://drive.google.com/file/d/1mf_xRKp0iZQSX_CIYDFomOqOmiLgN-4f/view?usp=sharing) ilustrates how to run them