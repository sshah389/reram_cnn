#!/bin/python3
import torch
from torch import nn, Tensor
import torch.optim as optim
import torchvision.transforms as transforms
import torchvision

import matplotlib.pyplot as plt
import numpy as np

import sys

from ReRAM import ReRAMNet, ReRAMLinear, ReRAMConv2d, test_fit
from ReRAM import get_fil_bins, get_fil_from_bin, quant_weight, quant_dac_adc, FIL_BINS0

from sklearn.metrics import confusion_matrix
import seaborn as sn
import pandas as pd


ENABLE_BIAS = True

# Patamaters (only used during testing and not training)
Q_STEP = 6  # weight quant. = log2(84/Q_STEP)
ADC_QUANT = 8
DAC_QUANT = 8



plt.rcParams['figure.figsize'] = [15, 15]

# here you can ovverride whether it uses GPU or CPU
device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
#write your tensor with device , example - data=data.to(device), this will take your tensors to the desired device

max_split_size_mb = 64

transform = transforms.Compose(
    [transforms.ToTensor(),
     transforms.Pad(2),
     transforms.Normalize((0.5,), (0.5,))])

batch_size = 32

trainset = torchvision.datasets.MNIST(root='./data', train=True,
                                        download=True, transform=transform)
trainloader = torch.utils.data.DataLoader(trainset, batch_size=batch_size,
                                          shuffle=True, num_workers=2)

testset = torchvision.datasets.MNIST(root='./data', train=False,
                                       download=True, transform=transform)
testloader = torch.utils.data.DataLoader(testset, batch_size=batch_size,
                                         shuffle=False, num_workers=2)

classes = ('0', '1', '2', '3',
           '4', '5', '6', '7', '8', '9')


# functions to show an image
def imshow(img):
    img = img / 2 + 0.5     # unnormalize
    npimg = img.numpy()
    plt.imshow(np.transpose(npimg, (1, 2, 0)))
    plt.show()



class ReRAMNetLeNet5(ReRAMNet):
  def __init__(self):
    super().__init__()

    self.conv1 = ReRAMConv2d(1, 6, 5, 1, bias=ENABLE_BIAS)
    self.maxPool = nn.MaxPool2d(2)
    self.conv2 = ReRAMConv2d(6, 16, 5, 1, bias=ENABLE_BIAS)
    self.conv3 = ReRAMConv2d(16, 120, 5, 1, bias=ENABLE_BIAS)

    self.fc1 = ReRAMLinear(120, 84, bias=ENABLE_BIAS)
    self.fc2 = ReRAMLinear(84, 10, bias=ENABLE_BIAS, no_act=True)
    
    self._register_reram_layer_('conv1', self.conv1)
    self._register_reram_layer_('conv2', self.conv2)
    self._register_reram_layer_('conv3', self.conv3)

    self._register_reram_layer_('fc1', self.fc1)
    self._register_reram_layer_('fc2', self.fc2)

  def forward(self, x):
    x = x / 2 + 0.5 # Normalize input

    x = self.input_quant_fn(x)
    x = self.conv1(x)
    x = self.output_quant_fn(x)

    x = self.maxPool(x)

    x = self.input_quant_fn(x)
    x = self.conv2(x)
    x = self.output_quant_fn(x)

    x = self.maxPool(x)

    x = self.input_quant_fn(x)
    x = self.conv3(x)
    x = self.output_quant_fn(x)

    x = torch.flatten(x, 1) # flatten all dims except batch

    x = self.input_quant_fn(x)
    x = self.fc1(x)
    x = self.output_quant_fn(x)
    x = self.input_quant_fn(x)
    x = self.fc2(x)
    x = self.output_quant_fn(x)
    
    return x


SELECTED_NET = ReRAMNetLeNet5

from torch._C import dtype
def train(PATH):

  # Traning
  net = SELECTED_NET()

  print(device)

  net.reset_current_measurment()

  net.to(device)

  criterion = nn.CrossEntropyLoss()
  #optimizer = optim.SGD(net.parameters(), lr=0.001, momentum=0.9)

  optimizer = optim.Adam(net.parameters(), lr=0.001, betas=(0.9, 0.999), eps=1e-08)

  NUM_EPOCHS = 50
  MAX_PATIENCE = 3 #TODO: implement patience
  best_loss = 1.0e100
  patience = MAX_PATIENCE
  stop = False
  L_MOD = 200

  loss_history = np.zeros((NUM_EPOCHS * int(len(trainloader)/L_MOD),))

  #PATH = sys.argv[1] if len(sys.argv) > 1 else 'train_reram_out.pth'

  li = 0
  for epoch in range(NUM_EPOCHS):  # loop over the dataset multiple times
      print(f'Running Epoch: {epoch + 1} out of {NUM_EPOCHS}')
      running_loss = 0.0
      for i, data in enumerate(trainloader, 0):
        # get the inputs; data is a list of [inputs, labels]
        #inputs, labels = data
        inputs, labels = data[0].to(device), data[1].to(device)

        #labels = nn.functional.one_hot(labels, num_classes=10).to(dtype=float)

        # if epoch == 0 and i < 10:
        #   print(f'max label: {labels.max()} min label: {labels.min()}')
        #   print(inputs.shape)
        #   print(labels[0])      

        # zero the parameter gradients
        optimizer.zero_grad()

        # forward + backward + optimize
        outputs = net(inputs)
        loss = criterion(outputs, labels)
        loss.backward()
        optimizer.step()

        l = loss.detach().item()
        
        # print statistics
        running_loss += l
        if i % L_MOD == L_MOD - 1:    # print every L_MOD mini-batches
            print(f'[{epoch + 1}, {i + 1:5d}] loss: {running_loss / 2000:.5f}')
    
            loss_history[li] = running_loss / L_MOD
            li += 1

            if running_loss < best_loss:
              best_loss = running_loss
              patience = MAX_PATIENCE
            else:
              patience -= 1

            if epoch % 10 == 0:
                torch.save(net.state_dict(), PATH + f'_checkpoint_{epoch}')

            if patience <= 0:
              #print("loss has stopped decreasing")
              #break
              pass

            running_loss = 0.0
      
      # Stop traning if the loss is not decreasing
      # TODO: implement this feature
      # if running_loss >= best_loss:
      #     patience -= 1
      #     print(f'Patience: {patience} / {MAX_PATIENCE}')
      #     if patience <= 0:
      #         break

        #return        
              

  print('Finished Training')

  net.print_avg_current()

  plt.plot(loss_history)

  torch.save(net.state_dict(), PATH)

def test(PATH):
  net = SELECTED_NET()
  
  fil_bins = get_fil_bins(FIL_BINS0, Q_STEP)
  
  net.enable_power_calc()
  net.reset_current_measurment()
  net.reset_power_measurment()

  #PATH = sys.argv[1] if len(sys.argv) > 1 else 'train_reram_out.pth'

  dataiter = iter(testloader)
  images, labels = dataiter.next()

  net.load_state_dict(torch.load(PATH, map_location=torch.device('cpu')))

  # Apply quantazation to trained weights
  fil_bins_count = [0] * len(fil_bins) # used for histogram later
  def qw(w):
    return quant_weight(fil_bins, w, fil_bins_count)
  net.quantize_weights(qw) # this function call is slow so not good to call too often

  def qa(x):
    return quant_dac_adc(x, ADC_QUANT)
  
  def qd(x):
    return quant_dac_adc(x, DAC_QUANT)

  # Set the quantization function for the inputs and outputs
  net.set_input_quant_fn(qd)
  net.set_output_quant_fn(qa)

  print(fil_bins_count)

  outputs = net(images)


  _, predicted = torch.max(outputs, 1)

  print('Predicted: ', ' '.join(f'{classes[predicted[j]]:5s}'
                                for j in range(4)))

  correct = 0
  total = 0
  # since we're not training, we don't need to calculate the gradients for our outputs
  with torch.no_grad():
      for data in testloader:
          images, labels = data
          # calculate outputs by running images through the network
          outputs = net(images)
          # the class with the highest energy is what we choose as prediction
          _, predicted = torch.max(outputs.data, 1)
          total += labels.size(0)
          correct += (predicted == labels).sum().item()

  print(f'Accuracy of the network on the 10000 test images: {100 * correct // total} %')

  net.print_avg_current()
  net.print_power_measurment()

  # prepare to count predictions for each class
  correct_pred = {classname: 0 for classname in classes}
  total_pred = {classname: 0 for classname in classes}

  # again no gradients needed
  with torch.no_grad():
      for data in testloader:
          images, labels = data
          #print(images.shape)
          outputs = net(images)
          _, predictions = torch.max(outputs, 1)
          # collect the correct predictions for each class
          for label, prediction in zip(labels, predictions):
              if label == prediction:
                  correct_pred[classes[label]] += 1
              total_pred[classes[label]] += 1


  # print accuracy for each class
  for classname, correct_count in correct_pred.items():
      accuracy = 100 * float(correct_count) / total_pred[classname]
      print(f'Accuracy for class: {classname:5s} is {accuracy:.1f} %')

  
  #net.weights_to_csv(PATH)
  #net.pulses_to_csv(PATH + 'pulses')

  return net, fil_bins_count


def plot(net, fil_bins_count):
  testloader = torch.utils.data.DataLoader(testset, batch_size=len(testset),
                                          shuffle=False, num_workers=2)
  y_pred = []
  y_true = []

  for data in testloader:
          images, labels = data
          print(images.shape)
          pred = net(images).detach()
          pred = (torch.max(torch.exp(pred), 1)[1]).data.cpu().numpy()
          y_pred.extend(pred)
          y_true.extend(labels)

  from numpy.ma.core import shape
  cf_mat = confusion_matrix(y_true, y_pred)

  cf_mat_norm = np.zeros(shape=cf_mat.shape, dtype=np.float64)

  # Normalize each col
  for iCol in range(len(cf_mat[0][:])):
    print(cf_mat[:][iCol])
    print(np.sum(cf_mat[:][iCol]))
    col = cf_mat[:][iCol]/np.sum(cf_mat[:][iCol])
    print(col)
    print("~~~~~")
    cf_mat_norm[:][iCol] = col

  df_cm = pd.DataFrame(cf_mat_norm, index = [i for i in classes],
                        columns = [ i for i in classes])
  print(df_cm)
  plt.figure(figsize = (12, 7))
  sn.heatmap(df_cm, annot=True)

  # Create histogram
  plt.figure()
  plt.plot(fil_bins_count)
  plt.title("Histogram of ReRAM filaments used in model")

  plt.show()


if __name__ == '__main__':

  if len(sys.argv) < 2:
    print("usage: ./LeNetTrainTest.py <train|test|both|tf> [path]")
    exit(-1)

  PATH = sys.argv[2] if len(sys.argv) > 2 else 'train_reram_out.pth'

  if sys.argv[1] == 'tf':
    test_fit()

  if sys.argv[1] == 'train' or sys.argv[1] == 'both':
    train(PATH)
  if sys.argv[1] == 'test' or sys.argv[1] == 'both':
    net, fil_bins_count = test(PATH)
    plot(net, fil_bins_count)