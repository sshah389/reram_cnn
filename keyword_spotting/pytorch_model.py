import torch
import torch.nn as nn
import torch.nn.functional as F
import sys

class DepthwiseSeparableCNN(nn.Module):
    def __init__(self, input_shape, num_classes, weight_decay=1e-4):
        super(DepthwiseSeparableCNN, self).__init__()
        self.num_classes = num_classes

        # Regular convolutional layer
        self.conv1 = nn.Conv2d(in_channels=input_shape[2], out_channels=64, kernel_size=(10, 4), stride=(2, 2), padding=1)
        self.bn1 = nn.BatchNorm2d(64)
        self.dropout1 = nn.Dropout(0.2)

        # Depthwise separable convolutional layers
        self.dw_conv_layers = nn.ModuleList([
            self._depthwise_separable_conv(64, 64, weight_decay) for _ in range(4)
        ])
        
        # Pooling and final layers
        self.avg_pool = nn.AdaptiveAvgPool2d((1, 1))
        self.flatten = nn.Flatten()
        self.final_dense = nn.Linear(64, num_classes)

    def _depthwise_separable_conv(self, nin, nout, weight_decay):
        depthwise = nn.Conv2d(nin, nin, kernel_size=3, padding=1, groups=nin, bias=False)
        pointwise = nn.Conv2d(nin, nout, kernel_size=1, bias=False)
        bn = nn.BatchNorm2d(nout)
        relu = nn.ReLU(inplace=True)
        return nn.Sequential(depthwise, pointwise, bn, relu, nn.Dropout(0.2))

    def forward(self, x):
        x = F.relu(self.bn1(self.conv1(x)))
        x = self.dropout1(x)
        
        for dw_layer in self.dw_conv_layers:
            x = dw_layer(x)
        
        x = self.avg_pool(x)
        x = self.flatten(x)
        x = self.final_dense(x)
        return x


class TDCNN(nn.Module):
    def __init__(self, input_shape, num_classes, filters=64, weight_decay=1e-4):
        super(TDCNN, self).__init__()
        print('Input shape; ', input_shape)
        self.conv1 = nn.Conv2d(input_shape[2], filters, (512, 1), stride=(384, 1), padding=0)
        self.bn1 = nn.BatchNorm2d(filters)
        self.dropout1 = nn.Dropout(0.2)

        # Second convolution layer
        self.conv2 = nn.Conv2d(1, filters, (10, 4), stride=(2, 2), padding=5)
        self.bn2 = nn.BatchNorm2d(filters)
        self.dropout2 = nn.Dropout(0.2)

        # Depthwise separable convolutions
        self.depthwise1 = nn.Conv2d(filters, filters, (3, 3), groups=filters, padding=(1, 1))
        self.pointwise1 = nn.Conv2d(filters, filters, (1, 1))
        
        self.depthwise2 = nn.Conv2d(filters, filters, (3, 3), groups=filters, padding=(1, 1))
        self.pointwise2 = nn.Conv2d(filters, filters, (1, 1))
        
        self.depthwise3 = nn.Conv2d(filters, filters, (3, 3), groups=filters, padding=(1, 1))
        self.pointwise3 = nn.Conv2d(filters, filters, (1, 1))
        
        self.depthwise4 = nn.Conv2d(filters, filters, (3, 3), groups=filters, padding=(1, 1))
        self.pointwise4 = nn.Conv2d(filters, filters, (1, 1))
        
        self.bn3 = nn.BatchNorm2d(filters)

        self.dropout_final = nn.Dropout(0.4)
        self.global_avg_pool = nn.AdaptiveAvgPool2d((1, 1))
        self.fc = nn.Linear(filters, num_classes)
    
    def forward(self, x):
        x = F.relu(self.bn1(self.conv1(x)))
        x = self.dropout1(x)
        
        x = x.permute(0, 3, 2, 1)
        
        x = F.relu(self.bn2(self.conv2(x)))
        x = self.dropout2(x)
        
        x = F.relu(self.bn3(self.depthwise1(x)))
        x = F.relu(self.bn3(self.pointwise1(x)))

        x = F.relu(self.bn3(self.depthwise2(x)))
        x = F.relu(self.bn3(self.pointwise2(x)))

        x = F.relu(self.bn3(self.depthwise3(x)))
        x = F.relu(self.bn3(self.pointwise3(x)))

        x = F.relu(self.bn3(self.depthwise4(x)))
        x = F.relu(self.bn3(self.pointwise4(x)))

        x = self.dropout_final(x)
        x = self.global_avg_pool(x)
        x = torch.flatten(x, 1)
        x = self.fc(x)
        return x


class FC4(nn.Module):
    def __init__(self, input_shape, num_classes):
        super(FC4, self).__init__()
        self.flatten = nn.Flatten()
        self.fc1 = nn.Linear(input_shape[0] * input_shape[1], 256)
        self.dropout1 = nn.Dropout(0.2)
        self.bn1 = nn.BatchNorm1d(256)
        
        self.fc2 = nn.Linear(256, 256)
        self.dropout2 = nn.Dropout(0.2)
        self.bn2 = nn.BatchNorm1d(256)
        
        self.fc3 = nn.Linear(256, 256)
        self.dropout3 = nn.Dropout(0.2)
        self.bn3 = nn.BatchNorm1d(256)
        
        self.fc4 = nn.Linear(256, num_classes)

    def forward(self, x):
        x = self.flatten(x)
        x = F.relu(self.bn1(self.fc1(x)))
        x = self.dropout1(x)
        x = F.relu(self.bn2(self.fc2(x)))
        x = self.dropout2(x)
        x = F.relu(self.bn3(self.fc3(x)))
        x = self.dropout3(x)
        x = (self.fc4(x))
        return x

def prepare_model_settings(label_count, args):
  """Calculates common settings needed for all models.
  Args:
    label_count: How many classes are to be recognized.
    sample_rate: Number of audio samples per second.
    clip_duration_ms: Length of each audio clip to be analyzed.
    window_size_ms: Duration of frequency analysis window.
    window_stride_ms: How far to move in time between frequency windows.
    dct_coefficient_count: Number of frequency bins to use for analysis.
  Returns:
    Dictionary containing common settings.
  """
  desired_samples = int(args.sample_rate * args.clip_duration_ms / 1000)
  if args.feature_type == 'td_samples':
    window_size_samples = 1
    spectrogram_length = desired_samples
    dct_coefficient_count = 1
    window_stride_samples = 1
    fingerprint_size = desired_samples
  else:
    dct_coefficient_count = args.dct_coefficient_count
    window_size_samples = int(args.sample_rate * args.window_size_ms / 1000)
    window_stride_samples = int(args.sample_rate * args.window_stride_ms / 1000)
    length_minus_window = (desired_samples - window_size_samples)
    if length_minus_window < 0:
      spectrogram_length = 0
    else:
      spectrogram_length = 1 + int(length_minus_window / window_stride_samples)
      fingerprint_size = args.dct_coefficient_count * spectrogram_length
  return {
    'desired_samples': desired_samples,
    'window_size_samples': window_size_samples,
    'window_stride_samples': window_stride_samples,
    'feature_type': args.feature_type, 
    'spectrogram_length': spectrogram_length,
    'dct_coefficient_count': dct_coefficient_count,
    'fingerprint_size': fingerprint_size,
    'label_count': label_count,
    'sample_rate': args.sample_rate,
    'background_frequency': 0.8, # args.background_frequency
    'background_volume_range_': 0.1
  }



def get_model(args):
  model_name = args.model_architecture

  label_count=12
  model_settings = prepare_model_settings(label_count, args)

  if model_name=="fc4":
    print("fc4 model invoked")
    
    model = FC4(input_shape = [model_settings['spectrogram_length'], model_settings['dct_coefficient_count'],1], num_classes=12)
  

  elif model_name == 'ds_cnn':
    print("DS CNN model invoked")
    
    model = DepthwiseSeparableCNN(input_shape = [model_settings['spectrogram_length'], model_settings['dct_coefficient_count'],1], num_classes=12)

  elif model_name == 'td_cnn':
    print("TD CNN model invoked")
    model = TDCNN(input_shape = [model_settings['spectrogram_length'], model_settings['dct_coefficient_count'],1], num_classes=12)
  else:
    raise ValueError("Model name {:} not supported".format(model_name))

  return model
