import pytorch_model as models
import get_dataset as kws_data
import kws_util
import torch
import torch.nn as nn
from torch.utils.data import DataLoader, Dataset
import numpy as np


num_classes = 12 # should probably draw this directly from the dataset.


Flags, unparsed = kws_util.parse_command()

print('We will download data to {:}'.format(Flags.data_dir))
print('We will train for {:} epochs'.format(Flags.epochs))
#Flags.model_architecture = 'td_cnn'
#Flags.feature_type = 'td_samples'

ds_train, ds_test, ds_val = kws_data.get_training_data(Flags)
print("Done getting data")

# this is taken from the dataset web page.
# there should be a better way than hard-coding this
train_shuffle_buffer_size = 85511
val_shuffle_buffer_size = 10102
test_shuffle_buffer_size = 4890


def tensorflow_to_pytorch(tf_dataset):
    features = []
    labels = []

    # Iterate through the TensorFlow dataset
    for batch in tf_dataset:
        feature_batch, label_batch = batch

        # Convert TensorFlow tensors to NumPy arrays and extend the list
        features.extend(feature_batch.numpy())  
        labels.extend(label_batch.numpy())    


    # Convert the list of all data points to PyTorch tensors and permute
    features = np.array(features)
    labels = np.array(labels)
    features = torch.tensor(features)
    features = features.permute(0, 3, 1, 2)
    labels = torch.tensor(labels)
    print(features.shape)
    print(labels.shape)


    return features, labels

class CustomDataset(Dataset):
    def __init__(self, features, labels):
        self.features = features
        self.labels = labels

    def __len__(self):
        return len(self.features)

    def __getitem__(self, index):
        return self.features[index], self.labels[index]
    


print("Train: ")
features, labels = tensorflow_to_pytorch(ds_train)
pytorch_dataset = CustomDataset(features, labels)
train_loader = DataLoader(pytorch_dataset, batch_size=32, shuffle=True)
print("Validation: ")
features_val, labels_val = tensorflow_to_pytorch(ds_val)
pytorch_dataset_val = CustomDataset(features_val, labels_val)
val_loader = DataLoader(pytorch_dataset_val, batch_size=32, shuffle=True)
print("Test: ")
features_test, labels_test = tensorflow_to_pytorch(ds_test)
pytorch_dataset_test = CustomDataset(features_test, labels_test)
test_loader = DataLoader(pytorch_dataset_test, batch_size=32, shuffle=True)


#model_name = "trainedModel.pth"
#Flags.model_init_path = "trained_models/" + model_name

if Flags.model_init_path is None:
  print("Starting with untrained model")
  model = models.get_model(args=Flags)
else:
  print(f"Starting with pre-trained model from {Flags.model_init_path}")
  model = models.get_model(args=Flags)
  model.load_state_dict(torch.load(Flags.model_init_path))



  


print(f"Model structure: {model}\n\n")

criterion = nn.CrossEntropyLoss()
optimizer = torch.optim.Adam(model.parameters(), lr=0.001)
num_epochs = Flags.epochs
for epoch in range(num_epochs):
    model.train()
    for data, targets in train_loader:
        optimizer.zero_grad()
        outputs = model(data)
        loss = criterion(outputs, targets)
        loss.backward()
        optimizer.step()


    model.eval()
    total = 0
    correct = 0
    with torch.no_grad():
        for data, targets in val_loader:
            outputs = model(data)
            _, predicted = torch.max(outputs, 1)
            total += targets.size(0)
            correct += (predicted == targets).sum().item()

    print(f'Epoch {epoch+1}: Accuracy: {100 * correct / total}%')


model_name = "trainedModel.pth"
Flags.model_init_path = "trained_models/" + model_name

torch.save(model.state_dict(), Flags.model_init_path)

if Flags.run_test_set:
  model.eval()
  total = 0
  correct = 0
  with torch.no_grad():
      for data, targets in test_loader:
          outputs = model(data)
          _, predicted = torch.max(outputs, 1)
          total += targets.size(0)
          correct += (predicted == targets).sum().item()

  print(f'Test accuracy: {100 * correct / total}%')
