import pytorch_model as models
import get_dataset as kws_data
import kws_util
import torch
import torch.nn as nn
from torch.utils.data import DataLoader, Dataset
import matplotlib.pyplot as plt
import numpy as np
import os
import argparse
import eval_functions_eembc as eembc_ev
from sklearn.metrics import roc_auc_score
from torch.nn.functional import softmax

if __name__ == '__main__':
    num_classes = 12 # should probably draw this directly from the dataset.


    Flags, unparsed = kws_util.parse_command()

    print('Data directory:  {:}'.format(Flags.data_dir))
    print('We will train for {:} epochs'.format(Flags.epochs))

    ds_train, ds_test, ds_val = kws_data.get_training_data(Flags)
    print("Done getting data")

    # this is taken from the dataset web page.
    # there should be a better way than hard-coding this
    train_shuffle_buffer_size = 85511
    val_shuffle_buffer_size = 10102
    test_shuffle_buffer_size = 4890


    def tensorflow_to_pytorch(tf_dataset):
        features = []
        labels = []

        # Iterate through the TensorFlow dataset
        for batch in tf_dataset:
            feature_batch, label_batch = batch

            # Convert TensorFlow tensors to NumPy arrays and extend the list
            features.extend(feature_batch.numpy())  
            labels.extend(label_batch.numpy())    


        # Convert the list of all data points to PyTorch tensors and permute
        features = np.array(features)
        labels = np.array(labels)
        features = torch.tensor(features)
        features = features.permute(0, 3, 1, 2)
        labels = torch.tensor(labels)


        return features, labels

    class CustomDataset(Dataset):
        def __init__(self, features, labels):
            self.features = features
            self.labels = labels

        def __len__(self):
            return len(self.features)

        def __getitem__(self, index):
            return self.features[index], self.labels[index]


    print("Test: ")
    features_test, labels_test = tensorflow_to_pytorch(ds_test)
    pytorch_dataset_test = CustomDataset(features_test, labels_test)
    test_loader = DataLoader(pytorch_dataset_test, batch_size=32, shuffle=True)

    model_name = "trainedModel.pth"
    Flags.model_init_path = "trained_models/" + model_name

    if Flags.model_init_path is None:
      print("Starting with untrained model")
      model = models.get_model(args=Flags)
    else:
      print(f"Starting with pre-trained model from {Flags.model_init_path}")
      model = models.get_model(args=Flags)
      model.load_state_dict(torch.load(Flags.model_init_path))


    print(f"Model structure: {model}\n\n")

    criterion = nn.CrossEntropyLoss()
    optimizer = torch.optim.Adam(model.parameters(), lr=0.001)
    num_epochs = Flags.epochs

    model.eval()
    total = 0
    correct = 0
    outputs = []
    labels = []
    with torch.no_grad():
        for data, targets in test_loader:
            output = model(data)
            probabilities = softmax(output, dim=1)
            _, predicted = torch.max(output, 1)
            total += targets.size(0)
            correct += (predicted == targets).sum().item()
            predicted_np = predicted.cpu().numpy()
            outputs.extend(probabilities)
            labels.extend(targets.cpu().numpy())

    print(f'Test accuracy: {100 * correct / total}%')

    outputs = np.array(outputs)
    labels = np.array(labels)
    
    print("==== EEMBC calculate_accuracy Method ====")
    accuracy_eembc = eembc_ev.calculate_accuracy(outputs, labels)
    print(40*"=")

    print("==== SciKit-Learn AUC ====")
    auc_scikit = roc_auc_score(labels, outputs, multi_class='ovr')
    print(f"AUC (sklearn) = {auc_scikit}")
    print(40*"=")
    
    print("==== EEMBC calculate_auc ====")
    label_names = ["go", "left", "no", "off", "on", "right",
                  "stop", "up", "yes", "silence", "unknown"]

    auc_eembc = eembc_ev.calculate_auc(outputs, labels, label_names, Flags.model_architecture)
    print("---------------------")
