import os
import time
import numpy as np
import torch
import torch.nn as nn
from torch.utils.data import Dataset, DataLoader
import torch.optim as optim
import librosa
from tqdm import tqdm

def setup_local_directory():
    """Create local directory for dataset"""
    dataset_dir = os.path.join(os.getcwd(), 'ToyADMOS_Dataset')
    os.makedirs(dataset_dir, exist_ok=True)
    return dataset_dir

def create_toyadmos_like_dataset():
    """Creates a dataset following ToyADMOS characteristics"""
    base_dir = setup_local_directory()

    if os.path.exists(os.path.join(base_dir, 'train_data.npy')) and \
       os.path.exists(os.path.join(base_dir, 'test_data.npy')):
        print("Processed dataset already exists locally")
        return base_dir

    print("Creating ToyADMOS-like dataset...")

    sr = 48000
    duration = 10
    n_samples_train = 1000
    n_samples_test = 200
    n_mels = 128

    def generate_machine_sound(is_normal=True):
        t = np.linspace(0, duration, int(sr * duration))
        base_freq = 100

        if is_normal:
            signal = np.sin(2 * np.pi * base_freq * t) + \
                    0.5 * np.sin(2 * np.pi * 2 * base_freq * t) + \
                    0.25 * np.sin(2 * np.pi * 3 * base_freq * t)
            signal += np.random.normal(0, 0.05, len(t))
        else:
            signal = np.sin(2 * np.pi * (base_freq + np.random.uniform(-20, 20)) * t) + \
                    np.random.normal(0, 0.3, len(t))

        return signal

    train_specs = []
    test_specs = []

    print("Generating training data...")
    for _ in tqdm(range(n_samples_train)):
        signal = generate_machine_sound(is_normal=True)
        mel_spec = librosa.feature.melspectrogram(y=signal, sr=sr, n_mels=n_mels)
        mel_spec = librosa.power_to_db(mel_spec, ref=np.max)
        train_specs.append(mel_spec)

    print("Generating test data...")
    n_normal = int(n_samples_test * 0.8)
    n_anomaly = n_samples_test - n_normal

    for _ in tqdm(range(n_normal)):
        signal = generate_machine_sound(is_normal=True)
        mel_spec = librosa.feature.melspectrogram(y=signal, sr=sr, n_mels=n_mels)
        mel_spec = librosa.power_to_db(mel_spec, ref=np.max)
        test_specs.append(mel_spec)

    for _ in tqdm(range(n_anomaly)):
        signal = generate_machine_sound(is_normal=False)
        mel_spec = librosa.feature.melspectrogram(y=signal, sr=sr, n_mels=n_mels)
        mel_spec = librosa.power_to_db(mel_spec, ref=np.max)
        test_specs.append(mel_spec)

    np.save(os.path.join(base_dir, 'train_data.npy'), np.array(train_specs))
    np.save(os.path.join(base_dir, 'test_data.npy'), np.array(test_specs))

    return base_dir

class ImprovedToyADMOSAutoencoder(nn.Module):
    def __init__(self, input_dim):
        super(ImprovedToyADMOSAutoencoder, self).__init__()
        self.encoder = nn.Sequential(
            nn.Linear(input_dim, 256),
            nn.BatchNorm1d(256),
            nn.ReLU(),
            nn.Linear(256, 128),
            nn.BatchNorm1d(128),
            nn.ReLU(),
            nn.Linear(128, 64),
            nn.BatchNorm1d(64),
            nn.ReLU(),
            nn.Linear(64, 32)
        )
        self.decoder = nn.Sequential(
            nn.Linear(32, 64),
            nn.BatchNorm1d(64),
            nn.ReLU(),
            nn.Linear(64, 128),
            nn.BatchNorm1d(128),
            nn.ReLU(),
            nn.Linear(128, 256),
            nn.BatchNorm1d(256),
            nn.ReLU(),
            nn.Linear(256, input_dim)
        )

    def forward(self, x):
        encoded = self.encoder(x)
        decoded = self.decoder(encoded)
        return decoded

class ADMOSDataset(Dataset):
    def __init__(self, data_dir, is_train=True):
        pattern = "train" if is_train else "test"
        data_path = os.path.join(data_dir, f"{pattern}_data.npy")
        self.data = np.load(data_path)
        self.data = self.data.reshape(self.data.shape[0], -1)
        self.data = torch.FloatTensor(self.data)

    def __len__(self):
        return len(self.data)

    def __getitem__(self, idx):
        return self.data[idx]

def train_improved_model(model, train_loader, test_loader, device, save_dir):
    criterion = nn.MSELoss()
    optimizer = optim.AdamW(model.parameters(), lr=0.001, weight_decay=0.01)
    scheduler = optim.lr_scheduler.ReduceLROnPlateau(optimizer, mode='min', factor=0.5, patience=5)

    num_epochs = 150
    best_loss = float('inf')

    for epoch in range(num_epochs):
        model.train()
        total_loss = 0
        for batch in train_loader:
            batch = batch.to(device)
            optimizer.zero_grad()
            outputs = model(batch)
            loss = criterion(outputs, batch)
            loss.backward()
            torch.nn.utils.clip_grad_norm_(model.parameters(), max_norm=1.0)
            optimizer.step()
            total_loss += loss.item()

        avg_loss = total_loss / len(train_loader)
        scheduler.step(avg_loss)

        if avg_loss < best_loss:
            best_loss = avg_loss
            torch.save(model.state_dict(), os.path.join(save_dir, 'best_model.pth'))

        if (epoch + 1) % 10 == 0:
            print(f'Epoch [{epoch+1}/{num_epochs}], Loss: {avg_loss:.4f}')

    return model

def evaluate_model(model, test_loader, device):
    model.eval()
    anomaly_scores = []
    start_time = time.time()

    with torch.no_grad():
        for batch in test_loader:
            batch = batch.to(device)
            outputs = model(batch)
            mse = torch.mean((outputs - batch)**2, dim=1)
            anomaly_scores.extend(mse.cpu().numpy().tolist())

    inference_time = time.time() - start_time
    anomaly_scores = np.array(anomaly_scores)
    threshold = np.percentile(anomaly_scores, 95)
    anomalies = np.where(anomaly_scores > threshold)[0]

    return anomaly_scores, threshold, anomalies, inference_time

if __name__ == "__main__":
    dataset_dir = create_toyadmos_like_dataset()
    print(f"Dataset is ready at: {dataset_dir}")

    train_dataset = ADMOSDataset(dataset_dir, is_train=True)
    test_dataset = ADMOSDataset(dataset_dir, is_train=False)

    batch_size = 16
    train_loader = DataLoader(train_dataset, batch_size=batch_size, shuffle=True)
    test_loader = DataLoader(test_dataset, batch_size=batch_size, shuffle=False)

    input_dim = train_dataset[0].shape[0]
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    model = ImprovedToyADMOSAutoencoder(input_dim).to(device)

    model = train_improved_model(model, train_loader, test_loader, device, dataset_dir)

    anomaly_scores, threshold, anomalies, inference_time = evaluate_model(model, test_loader, device)

    print(f"\nEvaluation Results:")
    print(f"Number of anomalies detected: {len(anomalies)}")
    print(f"Anomaly threshold: {threshold:.4f}")
    print(f"Inference time: {inference_time:.4f} seconds")

    save_path = os.path.join(dataset_dir, 'final_admos_model.pth')
    torch.save(model.state_dict(), save_path)
    print(f"\nModel saved to {save_path}")

## the dataset is not same as the tinyML benchmark. I encountered issues in downloading that dataset so, I simulated the dataset. 
