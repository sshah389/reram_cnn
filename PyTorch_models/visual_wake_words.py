import os
import json
import urllib.request
import zipfile
import torch
import torch.nn as nn
import numpy as np
from PIL import Image
from torch.utils.data import Dataset, DataLoader
from torchvision import transforms
from tqdm import tqdm
from pycocotools.coco import COCO

class DepthwiseSeparableConv(nn.Module):
    def __init__(self, in_channels, out_channels, stride=1):
        super(DepthwiseSeparableConv, self).__init__()
        self.depthwise = nn.Conv2d(in_channels, in_channels, kernel_size=3,
                                  stride=stride, padding=1, groups=in_channels, bias=False)
        self.pointwise = nn.Conv2d(in_channels, out_channels, kernel_size=1,
                                  stride=1, padding=0, bias=False)
        self.bn1 = nn.BatchNorm2d(in_channels, eps=0.001)
        self.bn2 = nn.BatchNorm2d(out_channels, eps=0.001)
        self.relu = nn.ReLU6(inplace=True)

    def forward(self, x):
        x = self.depthwise(x)
        x = self.bn1(x)
        x = self.relu(x)
        x = self.pointwise(x)
        x = self.bn2(x)
        x = self.relu(x)
        return x

class VisualWakeWordsNet(nn.Module):
    def __init__(self, num_classes=2):
        super(VisualWakeWordsNet, self).__init__()

        def conv_bn_relu(in_channels, out_channels, stride):
            return nn.Sequential(
                nn.Conv2d(in_channels, out_channels, kernel_size=3, stride=stride, padding=1, bias=False),
                nn.BatchNorm2d(out_channels, eps=0.001),
                nn.ReLU6(inplace=True)
            )

        self.initial_conv = conv_bn_relu(3, 32, stride=2)
        self.conv_dw_layers = nn.ModuleList([
            DepthwiseSeparableConv(32, 64, 1),
            DepthwiseSeparableConv(64, 128, 2),
            DepthwiseSeparableConv(128, 128, 1),
            DepthwiseSeparableConv(128, 256, 2),
            DepthwiseSeparableConv(256, 256, 1),
            DepthwiseSeparableConv(256, 512, 2),
        ])

        self.conv_dw_512 = nn.ModuleList([
            DepthwiseSeparableConv(512, 512, 1) for _ in range(5)
        ])

        self.final_layers = nn.ModuleList([
            DepthwiseSeparableConv(512, 1024, 2),
            DepthwiseSeparableConv(1024, 1024, 1)
        ])

        self.avg_pool = nn.AdaptiveAvgPool2d(1)
        self.fc = nn.Linear(1024, num_classes)

    def forward(self, x):
        x = self.initial_conv(x)
        for layer in self.conv_dw_layers:
            x = layer(x)
        for layer in self.conv_dw_512:
            x = layer(x)
        for layer in self.final_layers:
            x = layer(x)
        x = self.avg_pool(x)
        x = x.view(-1, 1024)
        x = self.fc(x)
        return x

def download_and_extract_coco():
    val_img_url = "http://images.cocodataset.org/zips/val2014.zip"
    val_ann_url = "http://images.cocodataset.org/annotations/annotations_trainval2014.zip"

    print("Downloading COCO validation images...")
    urllib.request.urlretrieve(val_img_url, "/content/coco/val2014.zip")
    print("Downloading COCO annotations...")
    urllib.request.urlretrieve(val_ann_url, "/content/coco/annotations.zip")

    print("Extracting validation images...")
    with zipfile.ZipFile("/content/coco/val2014.zip", 'r') as zip_ref:
        zip_ref.extractall("/content/coco")

    print("Extracting annotations...")
    with zipfile.ZipFile("/content/coco/annotations.zip", 'r') as zip_ref:
        zip_ref.extractall("/content/coco")

def create_visual_wake_words_annotations():
    coco = COCO('/content/coco/annotations/instances_val2014.json')
    vww_annotations = []

    print("Creating Visual Wake Words annotations...")
    for img_id in tqdm(coco.getImgIds()):
        img_info = coco.loadImgs(img_id)[0]
        ann_ids = coco.getAnnIds(imgIds=img_id)
        anns = coco.loadAnns(ann_ids)

        img_area = img_info['height'] * img_info['width']
        has_person = False

        for ann in anns:
            if ann['category_id'] == 1 and (ann['area'] / img_area) > 0.005:
                has_person = True
                break

        vww_annotations.append({
            'image_id': img_id,
            'file_name': img_info['file_name'],
            'label': 1 if has_person else 0
        })

    with open('/content/visual_wake_words/vww_annotations.json', 'w') as f:
        json.dump(vww_annotations, f)

    return vww_annotations

class VisualWakeWordsDataset(Dataset):
    def __init__(self, annotations, img_dir, transform=None):
        self.annotations = annotations
        self.img_dir = img_dir
        self.transform = transform

    def __len__(self):
        return len(self.annotations)

    def __getitem__(self, idx):
        ann = self.annotations[idx]
        img_path = os.path.join(self.img_dir, ann['file_name'])
        image = Image.open(img_path).convert('RGB')
        label = ann['label']

        if self.transform:
            image = self.transform(image)

        return image, label

def prepare_datasets():
    if not os.path.exists('/content/coco/val2014'):
        download_and_extract_coco()

    if not os.path.exists('/content/visual_wake_words/vww_annotations.json'):
        annotations = create_visual_wake_words_annotations()
    else:
        with open('/content/visual_wake_words/vww_annotations.json', 'r') as f:
            annotations = json.load(f)

    np.random.seed(42)
    indices = np.random.permutation(len(annotations))
    split = int(0.8 * len(annotations))
    train_indices = indices[:split]
    val_indices = indices[split:]

    train_annotations = [annotations[i] for i in train_indices]
    val_annotations = [annotations[i] for i in val_indices]

    train_transform = transforms.Compose([
        transforms.Resize((96, 96)),
        transforms.RandomHorizontalFlip(),
        transforms.ColorJitter(brightness=0.2, contrast=0.2, saturation=0.2),
        transforms.ToTensor(),
        transforms.Normalize(mean=[0.485, 0.456, 0.406],
                           std=[0.229, 0.224, 0.225])
    ])

    val_transform = transforms.Compose([
        transforms.Resize((96, 96)),
        transforms.ToTensor(),
        transforms.Normalize(mean=[0.485, 0.456, 0.406],
                           std=[0.229, 0.224, 0.225])
    ])

    train_dataset = VisualWakeWordsDataset(
        train_annotations,
        '/content/coco/val2014',
        transform=train_transform
    )

    val_dataset = VisualWakeWordsDataset(
        val_annotations,
        '/content/coco/val2014',
        transform=val_transform
    )

    return train_dataset, val_dataset

def train_model(model, train_loader, val_loader, criterion, optimizer, scheduler, num_epochs, device):
    model = model.to(device)
    best_acc = 0.0

    for epoch in range(num_epochs):
        model.train()
        running_loss = 0.0
        correct = 0
        total = 0

        pbar = tqdm(train_loader, desc=f'Epoch {epoch+1}/{num_epochs}')
        for inputs, labels in pbar:
            inputs = inputs.to(device)
            labels = labels.to(device)

            optimizer.zero_grad()
            outputs = model(inputs)
            loss = criterion(outputs, labels)
            loss.backward()
            optimizer.step()

            running_loss += loss.item()
            _, predicted = outputs.max(1)
            total += labels.size(0)
            correct += predicted.eq(labels).sum().item()

            pbar.set_postfix({
                'loss': f'{running_loss/len(train_loader):.4f}',
                'acc': f'{100.*correct/total:.2f}%'
            })

        model.eval()
        val_correct = 0
        val_total = 0

        with torch.no_grad():
            for inputs, labels in val_loader:
                inputs = inputs.to(device)
                labels = labels.to(device)
                outputs = model(inputs)
                _, predicted = outputs.max(1)
                val_total += labels.size(0)
                val_correct += predicted.eq(labels).sum().item()

        val_acc = 100. * val_correct / val_total
        print(f'Validation Accuracy: {val_acc:.2f}%')

        if val_acc > best_acc:
            best_acc = val_acc
            torch.save(model.state_dict(), 'best_model.pth')

        scheduler.step()

def main():
    print("Preparing datasets...")
    train_dataset, val_dataset = prepare_datasets()

    batch_size = 32
    num_epochs = 25
    learning_rate = 0.01
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

    train_loader = DataLoader(
        train_dataset,
        batch_size=batch_size,
        shuffle=True,
        num_workers=2,
        pin_memory=True
    )

    val_loader = DataLoader(
        val_dataset,
        batch_size=batch_size,
        shuffle=False,
        num_workers=2,
        pin_memory=True
    )

    model = VisualWakeWordsNet(num_classes=2)
    criterion = nn.CrossEntropyLoss()
    optimizer = torch.optim.SGD(
        model.parameters(),
        lr=learning_rate,
        momentum=0.9,
        weight_decay=4e-5
    )

    scheduler = torch.optim.lr_scheduler.StepLR(
        optimizer,
        step_size=30,
        gamma=0.1
    )

    print("Starting training...")
    train_model(model, train_loader, val_loader, criterion, optimizer, scheduler, num_epochs, device)

if __name__ == '__main__':
    main()
