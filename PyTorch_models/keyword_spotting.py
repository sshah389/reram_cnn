# Keyword Spotting Benchmark - TensorFlow to PyTorch Conversion


import torch
import torch.nn as nn
import torchaudio
import numpy as np
import matplotlib.pyplot as plt
from torch.utils.data import Dataset, DataLoader
import os
from torchaudio.datasets import SPEECHCOMMANDS

class SpeechCommandsDataset(Dataset):
    def __init__(self, split="training"):
        try:
            self.dataset = SPEECHCOMMANDS(
                root="/content/data",
                download=True,
                subset=split 
            )
            print(f"Dataset size: {len(self.dataset)}")
        except Exception as e:
            print(f"Error loading dataset: {e}")
            raise

        self.labels = ['yes', 'no', 'up', 'down', 'left', 'right',
                      'on', 'off', 'stop', 'go', 'silence', 'unknown']

        self.transform = torchaudio.transforms.MelSpectrogram(
            sample_rate=16000,
            n_mels=40,
            n_fft=480,
            hop_length=160
        )

        self.data = []
        self.targets = []

        print("Processing dataset...")
        for waveform, sample_rate, label, _, _ in self.dataset:
            if label not in self.labels[:-2]: # Exclude 'silence' and 'unknown'
                label = 'unknown'

            label_idx = self.labels.index(label)

            self.data.append(waveform)
            self.targets.append(label_idx)

        print(f"Processed {len(self.data)} samples")

    def __len__(self):
        return len(self.data)

    def __getitem__(self, idx):
        waveform = self.data[idx]
        label = self.targets[idx]

        mel_spec = self.transform(waveform)
        mel_spec = torchaudio.transforms.AmplitudeToDB()(mel_spec)

        mel_spec = mel_spec.squeeze() 

        return mel_spec, label

class KeywordSpottingModel(nn.Module):
    def __init__(self, num_classes=12):
        super(KeywordSpottingModel, self).__init__()

        self.relu = nn.ReLU()

        self.conv1 = nn.Conv2d(1, 64, kernel_size=(10, 4), stride=(2, 2))
        self.bn1 = nn.BatchNorm2d(64)
        self.conv2 = nn.Conv2d(64, 64, kernel_size=(3, 3), stride=(1, 1))
        self.bn2 = nn.BatchNorm2d(64)
        self.conv3 = nn.Conv2d(64, 32, kernel_size=(3, 3), stride=(1, 1))
        self.bn3 = nn.BatchNorm2d(32)
        self.conv4 = nn.Conv2d(32, 32, kernel_size=(3, 3), stride=(1, 1))
        self.bn4 = nn.BatchNorm2d(32)

        with torch.no_grad():
            self._calculate_flat_size()

        self.flatten = nn.Flatten()
        self.dense1 = nn.Linear(self.flat_size, 32)
        self.dropout = nn.Dropout(0.5)
        self.dense2 = nn.Linear(32, num_classes)

    def _calculate_flat_size(self):
        x = torch.randn(1, 1, 40, 101)
        x = self.relu(self.bn1(self.conv1(x)))
        x = self.relu(self.bn2(self.conv2(x)))
        x = self.relu(self.bn3(self.conv3(x)))
        x = self.relu(self.bn4(self.conv4(x)))
        self.flat_size = x.view(1, -1).size(1)
        print(f"Calculated flat size: {self.flat_size}")

    def forward(self, x):
        if x.dim() == 3:
            x = x.unsqueeze(1)

        x = self.relu(self.bn1(self.conv1(x)))
        x = self.relu(self.bn2(self.conv2(x)))
        x = self.relu(self.bn3(self.conv3(x)))
        x = self.relu(self.bn4(self.conv4(x)))

        x = self.flatten(x)
        x = self.relu(self.dense1(x))
        x = self.dropout(x)
        x = self.dense2(x)

        return x

def collate_fn(batch):
    """Custom collate function to handle variable-length spectrograms"""
    max_width = max(spec.size(-1) for spec, _ in batch)

    specs = []
    labels = []
    for spec, label in batch:
        pad_width = max_width - spec.size(-1)
        if pad_width > 0:
            spec = torch.nn.functional.pad(spec, (0, pad_width))
        specs.append(spec)
        labels.append(label)

    specs = torch.stack(specs)
    labels = torch.tensor(labels)

    return specs, labels

def validate_model(model, val_loader, criterion, device):
    model.eval()
    running_loss = 0.0
    correct = 0
    total = 0

    with torch.no_grad():
        for inputs, labels in val_loader:
            inputs, labels = inputs.to(device), labels.to(device)
            outputs = model(inputs)
            loss = criterion(outputs, labels)

            running_loss += loss.item()
            _, predicted = outputs.max(1)
            total += labels.size(0)
            correct += predicted.eq(labels).sum().item()

    return running_loss / len(val_loader), 100. * correct / total

def train_model(model, train_loader, val_loader, num_epochs=30, device='cuda'):
    criterion = nn.CrossEntropyLoss()
    optimizer = torch.optim.Adam(model.parameters(), lr=0.001)
    scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=20, gamma=0.1)

    best_accuracy = 0.0
    history = {'train_loss': [], 'train_acc': [], 'val_loss': [], 'val_acc': []}

    try:
        for epoch in range(num_epochs):
            # Training phase
            model.train()
            running_loss = 0.0
            correct = 0
            total = 0

            for batch_idx, (inputs, labels) in enumerate(train_loader):
                try:
                    inputs, labels = inputs.to(device), labels.to(device)

                    optimizer.zero_grad()
                    outputs = model(inputs)
                    loss = criterion(outputs, labels)

                    loss.backward()
                    optimizer.step()

                    running_loss += loss.item()
                    _, predicted = outputs.max(1)
                    total += labels.size(0)
                    correct += predicted.eq(labels).sum().item()

                    if batch_idx % 100 == 0:
                        print(f'Batch [{batch_idx}/{len(train_loader)}], '
                              f'Loss: {loss.item():.4f}')

                except Exception as e:
                    print(f"Error in batch {batch_idx}: {str(e)}")
                    continue

            scheduler.step()

            train_loss = running_loss / len(train_loader)
            train_acc = 100. * correct / total

            val_loss, val_acc = validate_model(model, val_loader, criterion, device)

            print(f'Epoch [{epoch+1}/{num_epochs}]')
            print(f'Train Loss: {train_loss:.4f}, Train Acc: {train_acc:.2f}%')
            print(f'Val Loss: {val_loss:.4f}, Val Acc: {val_acc:.2f}%')
            print(f'Learning Rate: {scheduler.get_last_lr()[0]:.6f}')

            history['train_loss'].append(train_loss)
            history['train_acc'].append(train_acc)
            history['val_loss'].append(val_loss)
            history['val_acc'].append(val_acc)

            if val_acc > best_accuracy:
                best_accuracy = val_acc
                torch.save(model.state_dict(), 'best_model.pth')

    except Exception as e:
        print(f"Training error: {str(e)}")
        raise

    return history

def main():
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    print(f"Using device: {device}")

    try:
        dataset = SpeechCommandsDataset(split="training")

        train_size = int(0.8 * len(dataset))
        val_size = len(dataset) - train_size
        train_dataset, val_dataset = random_split(dataset, [train_size, val_size])

        print(f"Train size: {train_size}, Val size: {val_size}")

        train_loader = DataLoader(
            train_dataset,
            batch_size=32,
            shuffle=True,
            num_workers=2,
            pin_memory=True,
            collate_fn=collate_fn
        )

        val_loader = DataLoader(
            val_dataset,
            batch_size=32,
            shuffle=False,
            num_workers=2,
            pin_memory=True,
            collate_fn=collate_fn
        )

        model = KeywordSpottingModel(num_classes=12)
        model = model.to(device)

        history = train_model(model, train_loader, val_loader, device=device)

        plt.figure(figsize=(12, 4))
        plt.subplot(1, 2, 1)
        plt.plot(history['train_loss'], label='Train Loss')
        plt.plot(history['val_loss'], label='Val Loss')
        plt.legend()
        plt.title('Loss History')

        plt.subplot(1, 2, 2)
        plt.plot(history['train_acc'], label='Train Acc')
        plt.plot(history['val_acc'], label='Val Acc')
        plt.legend()
        plt.title('Accuracy History')
        plt.show()

    except Exception as e:
        print(f"Error in main: {e}")
        raise

if __name__ == "__main__":
    main()
