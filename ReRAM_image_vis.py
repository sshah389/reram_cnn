
import sys

import torch


import matplotlib.pyplot as plt
import numpy as np
import csv

import torch.nn as nn
import torch.nn.functional as F



import json
from torch.nn.modules.module import register_module_forward_hook
from torch.nn import functional as F
from torch import Tensor
from torch.nn.common_types import _size_1_t, _size_2_t, _size_3_t
from torch.nn.modules.utils import _single, _pair
from torch.nn.modules.conv import _ConvNd
from torch.nn.modules.linear import Linear
from numpy import NaN

from typing import Union, Optional


# ReRAM Net Layers

#CURRENT_LIMIT = 10e-3

MAX_W = 1.0
MIN_W = -1.0
MAX_X = 1.0
MAX_Y = 1.0
MAX_V = 1.0
MAX_BIAS = 1.0
MIN_BIAS = -1.0
I_SAT = 1e-3 # Current limit
VDD = 1.8

# From reram VerilogA model obtaind from Skywater (sky130)
TFIL_MAX = 4.9e-9    # max filament in meters
TFIL_MIN = 3.3e-9    # min filament in meters
TFIL_REF = 4.7249e-9 # filament thickness calibration parameter
V_ref    = 0.430     # Voltage calibration paramter in Volts
I_k1     = 6.140e-5  # current calibration parameter in Amps
Tox      = 5.0e-9    # thickness of oxide in meters

# Max and min gap of conductive contact and filament
GAP_MAX = Tox - TFIL_MIN
GAP_MIN = Tox - TFIL_MAX

ALPHA = TFIL_MAX - TFIL_MIN


def simple_mult_op(x,w):
    return x * w

class ReRAMWeights(nn.Module):
    def __init__(self, weight: Tensor, op_func, bias: Optional[Tensor] = None,
                                                quantize_fn = None,
                                                calc_power = False,
                                                no_act = False):
        super(ReRAMWeights, self).__init__()

        self.weight = weight
        self.op_func = op_func
        self.bias = bias
        self.no_act = no_act

        self.first_feed = True

        self.n_samples = 0
        total_pos_curr = Tensor([0.0])
        total_neg_curr = Tensor([0.0])

        peak_pos_current = Tensor([0.0])
        peak_neg_current = Tensor([0.0])

        self.register_buffer('total_pos_curr', total_pos_curr, persistent=False)
        self.register_buffer('total_neg_curr', total_neg_curr, persistent=False)

        self.register_buffer('peak_pos_current', peak_pos_current, persistent=False)
        self.register_buffer('peak_neg_current', peak_neg_current, persistent=False)

        def identity(x: Tensor):
          return x

        self.quantize_fn = quantize_fn if quantize_fn else identity

        self.calc_power = False

        #TODO: combine n_samples and n_inferences into one variable
        self.n_inference = 0
        self.register_buffer('total_power', Tensor([0]), persistent=False)
        self.register_buffer('peak_power', Tensor([0]), persistent=False)

        # Used to calculate the transimpedance
        # base on MAX_INPUT * MAX_WEIGHT = MAX_OUTPUT
        # for example x = 1, w = 1 -> y = 1
        # everything is converted to a tensor to make the
        # self._forward function work with both these values
        # and the actual inputs (which have to be tensors)
        vMaxT = Tensor([MAX_V])
        wMaxT = Tensor([MAX_W])

        self.register_buffer('vMaxT', vMaxT)
        self.register_buffer('wMaxT', wMaxT)

        # Set the transimpedance
        max_I = self._forward(self.vMaxT, self.wMaxT, max_test=True)
        print(max_I)
        #max_I = Tensor([0.00001]) # override transimpedance

        # transimpedance boost (seems to help?)
        max_I /= 5

        self.register_buffer('transimpedance', (MAX_Y/max_I))

        print(f" transimpedance: {self.transimpedance}")

    def get_fil(self, weights, init_method='xavier', gain=1.0):
      """
      Get the positive and negative weight components for the analog CNN.

      Args:
          weights (np.ndarray or torch.Tensor): The weight tensor or ReRAMWeights object.
          init_method (str, optional): The weight initialization method to use. Default is 'xavier'.
          gain (float, optional): The gain value for Xavier initialization. Default is 1.0.

      Returns:
          list: A list containing the positive and negative weight components.
      """
      def xavier_init(shape, gain=1.0):
          """
          Xavier initialization of weights.

          Args:
              shape (tuple): The shape of the weight tensor.
              gain (float, optional): The gain value for the initialization. Default is 1.0.

          Returns:
              np.ndarray: The initialized weight tensor.
          """
          high = gain * np.sqrt(6.0 / np.sum(shape))
          low = -high
          return np.random.uniform(low=low, high=high, size=shape)

      if isinstance(weights, torch.Tensor):
          weights = weights.detach().cpu().numpy()
      elif isinstance(weights, ReRAMWeights):
          if init_method == 'xavier':
              # Assuming ReRAMWeights has a __len__ method that returns the number of weights
              num_weights = len(weights)
              # Assuming ReRAMWeights has a __getitem__ method that returns a single weight value
              weight_list = [weights[i] for i in range(num_weights)]
              shape = (num_weights,)
              weights = xavier_init(shape, gain=gain).reshape(weight_list)
          else:
              # Assuming ReRAMWeights has a get_weights method that returns a np.ndarray
              weights = weights.get_weights()

      # Clamp weights to reasonable ranges
      clamped_weights = np.maximum(np.minimum(weights, MAX_W), MIN_W)

      clamped_weights = torch.from_numpy(clamped_weights)

      # Scale and quantize positive weights
      pos = self.quantize_fn(clamped_weights * (ALPHA / MAX_W)) + TFIL_MIN

      # Scale and quantize negative weights
      neg = self.quantize_fn(-clamped_weights * (ALPHA / MAX_W)) + TFIL_MIN

      return [pos, neg]

    def _forward(self, inputs: Tensor, weights: Tensor, max_test = False):


        # Get the filament length for the current weight values
        # (split pos. and neg.).
        [w_fil_p, w_fil_n] = self.get_fil(weights)



        # Used for testing code
        w_lin_p = w_fil_p/TFIL_MAX
        w_lin_n = w_fil_n/TFIL_MAX


        # convert intput to voltage
        inputs = MAX_V * inputs/MAX_X;

        # The current through the ReRAM is caculated using the folowing
        # formula from the Skywater ReRAM model (also found in various
        # publications ).

        # I = I_k1 * exp(-(Tox - fil)/(Tox - TFIL_REF)) * sinh(V/V_ref)

        #print(f"inputs {inputs}")


        # Apply the sinh to the input
        sinhx = torch.sinh(inputs / V_ref)


        #print(f"sinhx {sinhx}")

        # selc.op_func is configured by the nn layer that is
        # using this ReRAMWeights module. Whaterver operation
        # the layer is performing self.op_func is used a "callback"
        # to perfrom said operations (e.g. Convolution, Fully Connected, ect)

        def simple_mult_op(x,w, bias):
          if bias == None:
            bias = 0
          return x * w + bias

        op = self.op_func
        G = 1.0
        if max_test:
          op = simple_mult_op
        else:
          # we are not training it so don't need to remeber
          # the call graph for the transimpedance
          G = self.transimpedance.detach()

        pos_bias = None
        neg_bias = None

        if self.bias != None and not max_test:
          # Convert the bias to a current
          pos_bias = (self.bias.clamp(0.0, MAX_BIAS)/MAX_BIAS)/G
          neg_bias = (self.bias.clamp(MIN_BIAS, 0.0)/MIN_BIAS)/G

        # Calculate the current/power through the ReRAM while
        # performing the nn operation (op is obtained from self.op_func)
        def calc_result(fx, fil, bias):
          return op(fx, I_k1 * torch.exp(-(Tox - fil)/(Tox - TFIL_REF)), bias)

        pos_current = calc_result(sinhx, w_fil_p, pos_bias)
        neg_current = calc_result(sinhx, w_fil_n, neg_bias)

        # Apply current t-gate saturation using tanh function
        pos_current = I_SAT * torch.tanh(pos_current/I_SAT)
        neg_current = I_SAT * torch.tanh(neg_current/I_SAT)
        if self.calc_power:
          # Power calc: P = V*I, where V is the input and I is the result of
          # "multiplying" the weight with V: I = fi(V) (fi is the non-linear ReRAM
          # voltage to current conversion). P = V * I = V * fi(V) = fp(V)

          xsinhx = inputs * sinhx

          pos_bias_power = None
          neg_bias_power = None

          # For including bias in power calc: assuming that Iin = Iout on
          # pfet curent source and P = Vin * Iin (bias = Iout, Vin = VDD)
          if self.bias != None and not max_test:
            pos_bias_power = pos_bias.mul(VDD)
            neg_bias_power = neg_bias.mul(VDD)
          pos_power = calc_result(xsinhx, w_fil_p, pos_bias_power)
          neg_power = calc_result(xsinhx, w_fil_n, neg_bias_power)

          # This assumes the batch is the 0th dim (batch dim = 0)
          batch_infernece_power = (pos_power.flatten(start_dim=1).sum(dim=1)
                          + neg_power.flatten(start_dim=1).sum(dim=1)).detach()

          if batch_infernece_power.max() > self.peak_power:
            self.peak_power = batch_infernece_power.max()

          self.total_power += batch_infernece_power.mean()

          self.n_inference += 1

        pcurr_max = pos_current.max().detach()
        ncurr_max = neg_current.max().detach()

        # use for debugging
        if not max_test and self.first_feed and False:
          self.first_feed = False

          print(f'self.peak_pos_current: {self.peak_pos_current.get_device()}')
          print(f'pcurr_max: {pcurr_max.get_device()}')

          print(f'self.total_pos_curr: {self.total_pos_curr.get_device()}') #-1
          print(f'pcurr_max.mean(): {pcurr_max.mean().get_device()}')   #0

          print(f'G: {G.get_device()}')

          print(f'Input Shape: {inputs.shape}')

        if self.peak_pos_current < pcurr_max:
            self.peak_pos_current = pcurr_max

        if self.peak_neg_current < ncurr_max:
            self.peak_neg_current = ncurr_max

        self.total_pos_curr += pcurr_max.mean().detach()
        self.total_neg_curr += ncurr_max.mean().detach()
        self.n_samples += 1

        if max_test:
          return pos_current - neg_current

        # These ReRAM layers can't have negative output
        # so the Relu function is used here
        # The transimpedance amplifier can't output
        # more than MAX_Y so tanh is used here
        #return torch.tanh((G * torch.relu(pos_current - neg_current))/MAX_Y) * MAX_Y

        #return torch.sigmoid(G * (pos_current - neg_current))
        if self.no_act:
          return G * (pos_current - neg_current)
        else:
          #print(f"pos_current: {pos_current} neg_current: {neg_current}")
          #r1 = torch.relu(G * (pos_current - neg_current))

          # Due to hardware limitations the ReRAM array has a fixed activation function
          # Move tensors to the same device
          pos_current = pos_current.to(device)
          neg_current = neg_current.to(device)
          r2 = torch.tanh( torch.relu(G * (pos_current - neg_current))/MAX_Y ) * MAX_Y
          r2=r2.to(device)
          return r2

    def forward(self, inputs: Tensor):
      inputs=inputs.to(device)
      return self._forward(inputs, self.weight, False)

    def set_quantize_fn(self, fn):
      def identity(x: Tensor):
          return x

      self.quantize_fn = fn if fn else identity

    def enable_power_calc(self):
      self.calc_power = True
      self.n_inference = 0

    def disable_power_calc(self):
      self.calc_power = False

    def reset_current_measurment(self):
      self.n_samples = 0
      self.total_pos_curr.fill_(0.0)
      self.total_neg_curr.fill_(0.0)

      self.peak_pos_current.fill_(0.0)
      self.peak_neg_current.fill_(0.0)

    def reset_power_measurment(self):
      self.n_infrences = 0

    def get_avg_current(self):
        if self.n_samples < 1:
            return [NaN, NaN]

        return [(self.total_pos_curr/self.n_samples).item(), (self.total_neg_curr/self.n_samples).item()]

    def get_peak_current(self):
        return [(self.peak_pos_current).item(), (self.peak_neg_current).item()]

    def get_avg_power(self):
      if self.n_inference < 1:
        return NaN

      return  (self.total_power/self.n_inference).item()

    def get_peak_power(self):
      return self.peak_power.item()

def test_fit():

    import numpy as np
    import matplotlib.pyplot as plt

    rw = ReRAMWeights(None, None)

    for i in np.arange(-1.0,1.0,0.005):
        x = Tensor(np.linspace(0,1))

        iT = Tensor(1)
        iT[0] = i

        y = rw._forward(x, torch.abs(iT), max_test=True)

        plt.plot(x,y*torch.sign(iT))
    plt.show()

class ReRAMConv2d(_ConvNd):
    def __init__(self,
                 in_channels: int,
                 out_channels: int,
                 kernel_size: _size_2_t,
                 stride: _size_2_t = 1,
                 padding: Union[str, _size_2_t] = 0,
                 dilation: _size_2_t = 1,
                 groups: int = 1,
                 bias: bool = True,
                 padding_mode: str = 'zeros',  # TODO: refine this type
                 device=None,
                 dtype=None) -> None:
        factory_kwargs = {'device': device, 'dtype': dtype}
        kernel_size_ = _pair(kernel_size)
        stride_ = _pair(stride)
        padding_ = padding if isinstance(padding, str) else _pair(padding)
        dilation_ = _pair(dilation)

        super(ReRAMConv2d, self).__init__(in_channels=in_channels,
                                          out_channels=out_channels,
                                          kernel_size=kernel_size_,
                                          stride=stride_,
                                          padding=padding_,
                                          dilation=dilation_,
                                          transposed=False, # TODO: allow this to not be false
                                          output_padding= _pair(0),
                                          groups=groups,
                                          bias=bias,
                                          padding_mode=padding_mode,
                                          **factory_kwargs)

        def op_func(input, weight, bias):
          # Move tensors to the same device
          input = input.to(weight.device)
          if bias is not None:
              bias = bias.to(weight.device)
          return self._conv_forward(input, weight, bias)

        self.rr_weights = ReRAMWeights(self.weight, op_func, self.bias)

    def _conv_forward(self, input: Tensor, weight: Tensor, bias: Optional[Tensor]):
     # Move input tensor to the same device as weight tensor
      input = input.to(weight.device)

      # Ensure data types match for input tensor and weight tensor
      if input.dtype != weight.dtype:
          input = input.type(weight.dtype)

      # Check if bias is provided and move it to the same device as input and weight tensors
      if bias is not None:
          bias = bias.to(weight.device)

          # Ensure data types match for bias tensor
          if input.dtype != bias.dtype:
              bias = bias.type(weight.dtype)
      return F.conv2d(input, weight, bias, self.stride,
                        self.padding, self.dilation, self.groups)

    def forward(self, input: Tensor) -> Tensor:
      return self.rr_weights(input)

    def reset_current_measurment(self):
      self.rr_weights.reset_current_measurment()

    def enable_power_calc(self):
      self.rr_weights.enable_power_calc()

    def reset_power_measurment(self):
      self.rr_weights.reset_power_measurment()

    def get_avg_current(self):
      return self.rr_weights.get_avg_current()

    def get_peak_current(self):
      return self.rr_weights.get_peak_current()

    def get_avg_power(self):
      return self.rr_weights.get_avg_power()

    def get_peak_power(self):
      return self.rr_weights.get_peak_power()

    def save_weights_as_json(self, filename):
      # [6, 1, 4, 4]
      # [16, 6, 4, 4]
      if not self.transposed:
        layer_data = {}
        layer_data['type'] = 'conv2d'
        layer_data['num_in_channels'] = self.in_channels
        layer_data['num_out_channels'] = self.out_channels
        layer_data['num_kernel_size'] = self.kernel_size

        layer_data['out_channels'] = []

        pos_pulses, neg_pulses = self.rr_weights.get_pulses(self.weight)

        for iOutChan in range(self.weight.shape[0]):
          out_chan = []
          for iInChan in range(self.weight.shape[1]):
            in_chan = {}
            in_chan['weight'] = []
            in_chan['pos_pulses'] = []
            in_chan['neg_pulses'] = []
            for x in range(self.weight.shape[2]):
              for y in range(self.weight.shape[3]):
                in_chan['weight'].append(self.weight[iOutChan, iInChan, x, y].tolist())
                in_chan['pos_pulses'].append(pos_pulses[iOutChan, iInChan, x, y].tolist())
                in_chan['neg_pulses'].append(neg_pulses[iOutChan, iInChan, x, y].tolist())
            out_chan.append(in_chan)
          layer_data['out_channels'].append(out_chan)
        with open(filename, 'w') as json_file:
          json.dump(layer_data, json_file, indent=2)

      else:
        print("Transposed is not yet supported!")




class ReRAMLinear(Linear):


  def __init__(self, in_features: int, out_features: int, bias: bool = True,
                 device=None, dtype=None, no_act = False) -> None:

    super(ReRAMLinear, self).__init__(in_features, out_features, bias,
                                      device, dtype)

    def op_func(input, weight, bias):
      # Move tensors to the same device
      input = input.to(weight.device)
      if bias is not None:
          bias = bias.to(weight.device)

      return F.linear(input, weight, bias)

    self.rr_weights = ReRAMWeights(self.weight, op_func, self.bias, no_act = no_act)

  def forward(self, input: Tensor) -> Tensor:
    return self.rr_weights(input)

  def reset_current_measurment(self):
      self.rr_weights.reset_current_measurment()

  def enable_power_calc(self):
    self.rr_weights.enable_power_calc()

  def reset_power_measurment(self):
    self.rr_weights.reset_power_measurment()

  def get_avg_current(self):
    return self.rr_weights.get_avg_current()

  def get_peak_current(self):
    return self.rr_weights.get_peak_current()

  def get_avg_power(self):
      return self.rr_weights.get_avg_power()

  def get_peak_power(self):
      return self.rr_weights.get_peak_power()

class ReRAMNet(nn.Module):
    def __init__(self):
        super().__init__()
        self.reram_layers = []
        self.input_quant_fn = lambda x: x
        self.output_quant_fn = lambda x: x

    def _register_reram_layer_(self, name, layer):
        self.reram_layers.append({'name':name, 'layer':layer})

    def set_quantize_fn(self, fn):
      for layer in self.reram_layers:
        layer['layer'].rr_weights.set_quantize_fn(fn)

    def set_input_quant_fn(self, fn):
      self.input_quant_fn = fn

    def set_output_quant_fn(self, fn):
      self.output_quant_fn = fn

    def quantize_weights(self, fn):
      for layer in self.reram_layers:
        layer['layer'].weight.detach().apply_(fn)

    def enable_power_calc(self):
      for layer in self.reram_layers:
        layer['layer'].enable_power_calc()

    def reset_current_measurment(self):
      for layer in self.reram_layers:
        layer['layer'].reset_current_measurment()

    def reset_power_measurment(self):
      for layer in self.reram_layers:
        layer['layer'].rr_weights.reset_power_measurment()

    #TODO: rename this to print_current_measurment
    def print_avg_current(self):

        for layer in self.reram_layers:
            [pos_avg_current, neg_avg_current] = layer['layer'].get_avg_current()
            [pos_peak_current, neg_peak_current] = layer['layer'].get_peak_current()

            print(f'layer: {layer["name"]}')
            print(f'\t\t pos weights avg current: {pos_avg_current * 1000.0} mA neg weights avg current: {neg_avg_current * 1000.0} mA')
            print(f'\t\t pos weights peak current: {pos_peak_current * 1000.0} mA neg weights peak current: {neg_peak_current * 1000.0} mA')

    def print_power_measurment(self):
      for layer in self.reram_layers:
        avg_power = layer['layer'].get_avg_power()
        peak_power = layer['layer'].get_peak_power()

        print(f'layer: {layer["name"]} \t\t avg power:' +
              f' {avg_power} W, peak power: {peak_power} W')

    def weights_to_csv(self, filename):
      for layer in self.reram_layers:
        with open(filename + f'_{layer["name"]}_.csv', 'w') as f:
          print('ran')
          writer = csv.writer(f)
          l = layer['layer']
          for i in range(l.weight.shape[0]):
            writer.writerow(l.weight[i].data)

    #TODO: this function is broken
    def pulses_to_csv(self, filename):
      for layer in self.reram_layers:
        with open(filename + f'_{layer["name"]}_.csv', 'w') as f:
          print('ran')
          writer = csv.writer(f)
          l = layer['layer']
          row = []
          for i in range(l.weight.shape[0]):
            for j in range(l.weight.shape[1]):
              pp,pn = l.rr_weights.get_pulses(l.rr_weights.weight)
              row.append(pp.data)
              row.append(pn.data)
            writer.writerow(row)



# Quantization for the ReRAM filiments
# These are filiment lengths obtained from the Sky130 ReRAM simulation model
# using a fixed pulse width and voltage. Each length is obtained after a single pulse
# Although the actual devices will have different lengths the idea here is to capture the
# distrubtion of the lengths (e.g. more high reistance states v.s. low resistance states)
#gap_bins = [9.99999999999998e-11, 1.1069615381801804e-10, 1.2257893197818198e-10, 1.3549354735909087e-10, 1.4943830120090899e-10, 1.644108158745452e-10, 1.804079135472728e-10, 1.974254043409089e-10, 2.154578347545457e-10, 2.344981946909089e-10, 2.545375760545457e-10, 2.755647873636365e-10, 2.9756592339090926e-10, 3.205238864636365e-10, 3.4441787671818227e-10, 3.6922284916363683e-10, 3.949089586727268e-10, 4.2144101283636385e-10, 4.4877795064545456e-10, 4.768723809999998e-10, 5.05670222418182e-10, 5.351104688999996e-10, 5.651251318636361e-10, 5.956393962181812e-10, 6.265720175545449e-10, 6.578359864000003e-10, 6.893394642545445e-10, 7.209869889090906e-10, 7.526809022454546e-10, 7.843229620272739e-10, 8.158160623454549e-10, 8.470659724181815e-10, 8.779830174272729e-10, 9.08483598581818e-10, 9.384914940636358e-10, 9.679388655000002e-10, 9.967669569545461e-10, 1.0249264535727256e-09, 1.0523775376090901e-09, 1.0790896522636357e-09, 1.1050410370909104e-09, 1.130218063818181e-09, 1.1546144788181828e-09, 1.1782305333636353e-09, 1.2010721065454545e-09, 1.22314982909091e-09, 1.244478250454546e-09, 1.2650750525454554e-09, 1.284960374272727e-09, 1.3041561648181803e-09, 1.3226856448181804e-09, 1.3405728460000014e-09, 1.3578422275892877e-09, 1.3743264375454534e-09, 1.3904432083928561e-09, 1.4058384684545468e-09, 1.4208965391964298e-09, 1.4352932989090926e-09, 1.4493822991964272e-09, 1.4628659594545442e-09, 1.4760697197321435e-09, 1.4887196177272716e-09, 1.501115489732145e-09, 1.5130044298181807e-09, 1.5246631087499992e-09, 1.535857419636361e-09, 1.5468430598214264e-09, 1.5574028199999985e-09, 1.5677734398214296e-09, 1.5777528900000042e-09, 1.5875607699107172e-09, 1.5970087699999995e-09, 1.6063009700000001e-09, 1.6152614900000022e-09, 1.624080359911506e-09, 1.632592949999996e-09, 1.6409765699999976e-09, 1.6490768400000009e-09, 1.657059500000002e-09, 1.6647795199090948e-09, 1.6723920997321456e-09, 1.6797607699999964e-09, 1.6870311499999995e-09, 1.694074589999998e-09]
FIL_BINS0 = [3.305925410000002e-09, 3.3129688500000004e-09, 3.320239230000004e-09, 3.3276079002678543e-09, 3.3352204800909056e-09, 3.342940499999998e-09, 3.350923159999999e-09, 3.3590234300000025e-09, 3.367407050000004e-09, 3.3759196400884944e-09, 3.3847385099999977e-09, 3.39369903e-09, 3.4029912300000008e-09, 3.412439230089283e-09, 3.422247109999996e-09, 3.4322265601785707e-09, 3.4425971800000016e-09, 3.4531569401785735e-09, 3.4641425803636393e-09, 3.475336891250001e-09, 3.4869955701818192e-09, 3.498884510267855e-09, 3.5112803822727287e-09, 3.5239302802678567e-09, 3.5371340405454557e-09, 3.550617700803573e-09, 3.5647067010909073e-09, 3.5791034608035703e-09, 3.5941615315454533e-09, 3.609556791607144e-09, 3.6256735624545465e-09, 3.6421577724107124e-09, 3.6594271539999985e-09, 3.6773143551818197e-09, 3.69584383518182e-09, 3.7150396257272732e-09, 3.7349249474545445e-09, 3.755521749545454e-09, 3.77685017090909e-09, 3.7989278934545454e-09, 3.821769466636365e-09, 3.845385521181817e-09, 3.869781936181819e-09, 3.8949589629090895e-09, 3.920910347736364e-09, 3.94762246239091e-09, 3.975073546427275e-09, 4.003233043045454e-09, 4.0320611345e-09, 4.061508505936364e-09, 4.091516401418182e-09, 4.122016982572727e-09, 4.1529340275818186e-09, 4.184183937654546e-09, 4.215677037972726e-09, 4.247319097754545e-09, 4.2790130110909094e-09, 4.310660535745455e-09, 4.3421640136e-09, 4.373427982445455e-09, 4.4043606037818185e-09, 4.434874868136364e-09, 4.464889531100001e-09, 4.4943297775818185e-09, 4.523127619000001e-09, 4.551222049354545e-09, 4.578558987163636e-09, 4.605091041327273e-09, 4.6307771508363635e-09, 4.655582123281818e-09, 4.679476113536364e-09, 4.702434076609091e-09, 4.724435212636364e-09, 4.745462423945454e-09, 4.765501805309091e-09, 4.784542165245455e-09, 4.802574595659091e-09, 4.819592086452727e-09, 4.835589184125455e-09, 4.850561698799091e-09, 4.86450645264091e-09, 4.877421068021818e-09, 4.889303846181982e-09, 4.9e-09]

# quantization = log2(len(fil0)/q_step)
def get_fil_bins(fil0, q_step):
    fil_bins = []
    # quantize to Q_STEP
    for i in range(0, len(fil0),q_step):
        fil_bins.append(fil0[i])

    return fil_bins

def bin_search(array, fil, off):
  l = len(array)
  if (l == 2):
    a = abs(array[0] - fil)
    b = abs(array[1] - fil)
    if (a>b):
     return (array[1], off + 0)
    else:
      return (array[0], off + 1)
  elif (l == 1):
    return array[0]

  i = int(l/2)

  if fil > array[i]:
    return bin_search(array[i:], fil, off + i)
  elif fil < array[i]:
    return bin_search(array[:i+1], fil, off)
  else:
    return (array[i], off + i)

def get_fil_from_bin(fil_bins, fil):
  return bin_search(fil_bins, fil, 0)


def quant_weight(fil_bins, w, fil_bins_count):
  out = 0
  if w > 0.0:
    fil = (min(w,1.0)/MAX_W)*ALPHA + TFIL_MIN
    filb, i = get_fil_from_bin(fil_bins, fil)
    fil_bins_count[i] += 1
    out = MAX_W*(filb-TFIL_MIN)/ALPHA
    #print(f"fil pos: {fil} -> {filb}")
  else:
    fil = (min(-w,1.0)/MAX_W)*ALPHA + TFIL_MIN
    filb, i = get_fil_from_bin(fil_bins, fil)
    fil_bins_count[i] += 1
    out = -MAX_W*(filb-TFIL_MIN)/ALPHA
    #print(f"fil neg: {fil} -> {filb}")

  return out


def quant_dac_adc(x: Tensor, num_bits):
  #assumes the range of x is [0 1]
  max = 1 << num_bits

  x = x.mul(max)

  x = x.round()

  max_1 = 1.0/(max)

  x = x.mul(max_1)

  return x

net = None

import torch
import torchvision
import torchvision.transforms as transforms
import torch.nn as nn
import torch.optim as optim
import matplotlib.pyplot as plt
import torch.nn.functional as F
from torch.optim.lr_scheduler import StepLR

from sklearn.metrics import confusion_matrix
import seaborn as sn
import pandas as pd
ENABLE_BIAS = True
Q_STEP = 6  # weight quant. = log2(84/Q_STEP)
ADC_QUANT = 8
DAC_QUANT = 8
plt.rcParams['figure.figsize'] = [15, 15]
batch_size = 32

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

import torch
from torchvision import datasets, transforms
transform = transforms.Compose(
    [transforms.ToTensor(),
     transforms.Pad(2),
     transforms.Normalize((0.5,), (0.5,))])

import torch
from torchvision import datasets, transforms

trainset = datasets.CIFAR10(root='./data', train=True, download=True, transform=transform)
trainloader = torch.utils.data.DataLoader(trainset, batch_size=batch_size, shuffle=True, num_workers=2)

testset = datasets.CIFAR10(root='./data', train=False, download=True)
testloader = torch.utils.data.DataLoader(testset, batch_size=batch_size, shuffle=False,num_workers=2)

# ReRAM NEtwork

import torch
import torch.nn as nn
import torch.nn.functional as F

class ReRAMTest(ReRAMNet):
    def __init__(self):
        super().__init__()
        # Resnet parameters
        self.num_filters = 16
        self.num_classes = 10

        # Input layer
        self.conv1 = ReRAMConv2d(3, self.num_filters, kernel_size=3, stride=1, padding=1,bias=ENABLE_BIAS)
        self.bn1 = nn.BatchNorm2d(self.num_filters)

        # First stack
        self.conv2 = ReRAMConv2d(self.num_filters, self.num_filters, kernel_size=3, stride=1, padding=1,bias=ENABLE_BIAS)
        self.bn2 = nn.BatchNorm2d(self.num_filters)
        self.conv3 = ReRAMConv2d(self.num_filters, self.num_filters, kernel_size=3, stride=1, padding=1,bias=ENABLE_BIAS)
        self.bn3 = nn.BatchNorm2d(self.num_filters)

        # Second stack
        self.num_filters *= 2
        self.conv4 = ReRAMConv2d(self.num_filters // 2, self.num_filters, kernel_size=3, stride=2, padding=1,bias=ENABLE_BIAS)
        self.bn4 = nn.BatchNorm2d(self.num_filters)
        self.conv5 = ReRAMConv2d(self.num_filters, self.num_filters, kernel_size=3, stride=1, padding=1,bias=ENABLE_BIAS)
        self.bn5 = nn.BatchNorm2d(self.num_filters)
        self.conv6 = ReRAMConv2d(self.num_filters // 2, self.num_filters, kernel_size=1, stride=2,bias=ENABLE_BIAS)
        self.bn6 = nn.BatchNorm2d(self.num_filters)

        # Third stack
        self.num_filters *= 2
        self.conv7 = ReRAMConv2d(self.num_filters // 2, self.num_filters, kernel_size=3, stride=2, padding=1,bias=ENABLE_BIAS)
        self.bn7 = nn.BatchNorm2d(self.num_filters)
        self.conv8 = ReRAMConv2d(self.num_filters, self.num_filters, kernel_size=3, stride=1, padding=1,bias=ENABLE_BIAS)
        self.bn8 = nn.BatchNorm2d(self.num_filters)
        self.conv9 = ReRAMConv2d(self.num_filters // 2, self.num_filters, kernel_size=1, stride=2,bias=ENABLE_BIAS)
        self.bn9 = nn.BatchNorm2d(self.num_filters)

        # Final classification layer
        self.pool_size = 8  # Pool size for CIFAR-10 images
        self.avg_pool = nn.AvgPool2d(self.pool_size)
        self.fc = ReRAMLinear(self.num_filters, self.num_classes,bias=ENABLE_BIAS)


        self._register_reram_layer_('conv1', self.conv1)
        self._register_reram_layer_('conv2', self.conv2)
        self._register_reram_layer_('conv3', self.conv3)
        self._register_reram_layer_('conv4', self.conv4)
        self._register_reram_layer_('conv5', self.conv5)
        self._register_reram_layer_('conv6', self.conv6)
        self._register_reram_layer_('conv7', self.conv7)
        self._register_reram_layer_('conv8', self.conv8)
        self._register_reram_layer_('conv9', self.conv9)

        self._register_reram_layer_('fc', self.fc)


    def forward(self, x):
        x = x.to(self.conv1.weight.device)
        # Input layer
        x = self.input_quant_fn(x)
        x = F.relu(self.bn1(self.conv1(x)))
        x = self.output_quant_fn(x)

        # First stack
        x = self.input_quant_fn(x)
        y = F.relu(self.bn2(self.conv2(x)))
        y = self.bn3(self.conv3(y))
        x = F.relu(x + y)
        x = self.output_quant_fn(x)

        # Second stack
        x = self.input_quant_fn(x)
        y = F.relu(self.bn4(self.conv4(x)))
        y = F.relu(self.bn5(self.conv5(y)))
        x = self.bn6(self.conv6(x))
        x = F.relu(x + y)
        x = self.output_quant_fn(x)

        # Third stack
        x = self.input_quant_fn(x)
        y = F.relu(self.bn7(self.conv7(x)))
        y = F.relu(self.bn8(self.conv8(y)))
        x = self.bn9(self.conv9(x))
        x = F.relu(x + y)
        x = self.output_quant_fn(x)

        # Final classification layer
        x = self.input_quant_fn(x)
        x = self.avg_pool(x)
        x = x.view(-1, self.num_filters)
        x = self.fc(x)
        x = self.output_quant_fn(x)

        return x






criterion = nn.CrossEntropyLoss()

net=ReRAMTest()

net = net.to(torch.device('cuda'))


def train_model(net, trainloader, testloader, criterion, initial_lr=0.001, lr_schedule=None, weight_decay=0, num_epochs=50):
    # net=ReRAMTest()
    net=net.to(device)
    # optimizer = optim.SGD(net.parameters(), lr=0.0001)
    net.reset_current_measurment()



    optimizer = optim.Adam(net.parameters(), lr=initial_lr, weight_decay=weight_decay)

    if lr_schedule:
        scheduler = lr_schedule(optimizer, initial_lr)

    for epoch in range(num_epochs):
        running_loss = 0.0
        total= 0
        correct=0
        for i, data in enumerate(trainloader, 0):
            inputs, labels = data[0].to(device), data[1].to(device)



            optimizer.zero_grad()

            outputs = net(inputs)
            loss = criterion(outputs, labels)
            loss.backward()

            _, predicted = torch.max(outputs.data, 1)
            total += labels.size(0)
            correct += (predicted == labels).sum().item()

            # Print gradients for each parameter
            # for name, param in net.named_parameters():
            #     if param.grad is not None:
            #         print(f'{name}.grad:', param.grad.norm().item())

            optimizer.step()

            running_loss += loss.item()
            if i % 20== 19:  # print every 20 mini-batches
            #   for name, param in net.named_parameters():
            #     if param.grad is not None:
            #         print(f'{name}.grad:', param.grad.norm().item())

              train_accuracy= 100*correct/100
              print('[%d, %5d] loss: %.3f ' %
                    (epoch + 1, i + 1, running_loss/20))
              running_loss = 0.0

        if lr_schedule:
            scheduler.step()



    print('Finished Training')


    print('Finished Training')


def lr_schedule(optimizer, initial_lr):
    return StepLR(optimizer, step_size=5, gamma=0.1)  # Reduce learning rate by a factor of 0.1 every 5 epochs




# Train the model
train_model(net, trainloader, testloader, criterion, initial_lr=0.001, lr_schedule=lr_schedule, weight_decay=0.0001, num_epochs=50)
