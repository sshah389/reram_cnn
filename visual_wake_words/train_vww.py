import os
os.environ["KMP_DUPLICATE_LIB_OK"] = "TRUE"
from absl import app
from pytorch_model import get_model
import torch
import torch.nn as nn
from torchvision import datasets, transforms
from torch.utils.data import DataLoader, random_split


IMAGE_SIZE = 96
BATCH_SIZE = 32
EPOCHS = 1

BASE_DIR = './vw_coco2014_96'

def main(argv):
  
  model = get_model()

  validation_split = 0.1

  train_transforms = transforms.Compose([
      transforms.RandomRotation(10),
      transforms.RandomResizedCrop(IMAGE_SIZE, scale=(0.95, 1.05)),
      transforms.RandomHorizontalFlip(),
      transforms.ToTensor()
  ])

  val_transforms = transforms.Compose([
      transforms.Resize(IMAGE_SIZE),
      transforms.CenterCrop(IMAGE_SIZE),
      transforms.ToTensor()
  ])

  dataset = datasets.ImageFolder(BASE_DIR, transform=None)

  # Split the dataset into training and validation sets
  val_size = int(len(dataset) * validation_split)
  train_size = len(dataset) - val_size
  train_dataset, val_dataset = random_split(dataset, [train_size, val_size])

  

  # Apply transformations to the training and validation datasets
  train_dataset.dataset.transform = train_transforms
  val_dataset.dataset.transform = val_transforms

  # Create DataLoaders for training and validation datasets
  train_loader = DataLoader(train_dataset, batch_size=BATCH_SIZE, shuffle=True)
  val_loader = DataLoader(val_dataset, batch_size=BATCH_SIZE, shuffle=False)  

  criterion = nn.CrossEntropyLoss()
  optimizer = torch.optim.Adam(model.parameters(), lr=0.001)
  num_epochs = EPOCHS
  total_len = len(train_loader)
  for epoch in range(num_epochs):
    print('Starting epoch',epoch)

    for batch, (data, targets) in enumerate(train_loader):
      batch +=1
      optimizer.zero_grad()
      outputs = model(data)
      loss = criterion(outputs, targets)
      loss.backward()
      optimizer.step()

      if batch%1000 == 0:
        model.eval()
        total = 0
        correct = 0
        with torch.no_grad():
          for data, targets in val_loader:
            outputs = model(data)
            _, predicted = torch.max(outputs, 1)
            total += targets.size(0)
            correct += (predicted == targets).sum().item()
        print(f'{batch/total_len} % done, Accuracy: {100 * correct / total}%')
        model.train()
      elif batch%200 == 0:
        print(batch/total_len,'% done')

  
  model_path = "./trainedModel.pth"
  torch.save(model.state_dict(), model_path)


if __name__ == '__main__':
  app.run(main)
