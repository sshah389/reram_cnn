import torch
import torch.nn as nn
import torch.nn.functional as F

class DepthwiseConv2d(nn.Module):
    def __init__(self, in_channels, kernel_size, stride=1, padding=0, bias=False):
        super(DepthwiseConv2d, self).__init__()
        self.depthwise = nn.Conv2d(in_channels, in_channels, kernel_size=kernel_size, 
                                   stride=stride, padding=padding, groups=in_channels, bias=bias)

    def forward(self, x):
        return self.depthwise(x)

class MobileNetV1(nn.Module):
    def __init__(self, num_classes=2, num_filters=8):
        super(MobileNetV1, self).__init__()
        
        self.conv1 = nn.Conv2d(3, num_filters, kernel_size=3, stride=2, padding=1, bias=False)
        self.bn1 = nn.BatchNorm2d(num_filters)
        
        self.depthwise2 = DepthwiseConv2d(num_filters, kernel_size=3, stride=1, padding=1)
        self.bn2 = nn.BatchNorm2d(num_filters)
        self.pointwise2 = nn.Conv2d(num_filters, num_filters * 2, kernel_size=1, stride=1, padding=0, bias=False)
        self.bn3 = nn.BatchNorm2d(num_filters * 2)
        
        self.depthwise3 = DepthwiseConv2d(num_filters * 2, kernel_size=3, stride=2, padding=1)
        self.bn4 = nn.BatchNorm2d(num_filters * 2)
        self.pointwise3 = nn.Conv2d(num_filters * 2, num_filters * 4, kernel_size=1, stride=1, padding=0, bias=False)
        self.bn5 = nn.BatchNorm2d(num_filters * 4)
        
        self.depthwise4 = DepthwiseConv2d(num_filters * 4, kernel_size=3, stride=1, padding=1)
        self.bn6 = nn.BatchNorm2d(num_filters * 4)
        self.pointwise4 = nn.Conv2d(num_filters * 4, num_filters * 4, kernel_size=1, stride=1, padding=0, bias=False)
        self.bn7 = nn.BatchNorm2d(num_filters * 4)
        
        self.depthwise5 = DepthwiseConv2d(num_filters * 4, kernel_size=3, stride=2, padding=1)
        self.bn8 = nn.BatchNorm2d(num_filters * 4)
        self.pointwise5 = nn.Conv2d(num_filters * 4, num_filters * 8, kernel_size=1, stride=1, padding=0, bias=False)
        self.bn9 = nn.BatchNorm2d(num_filters * 8)
        
        self.depthwise6 = DepthwiseConv2d(num_filters * 8, kernel_size=3, stride=1, padding=1)
        self.bn10 = nn.BatchNorm2d(num_filters * 8)
        self.pointwise6 = nn.Conv2d(num_filters * 8, num_filters * 8, kernel_size=1, stride=1, padding=0, bias=False)
        self.bn11 = nn.BatchNorm2d(num_filters * 8)
        
        self.depthwise7 = DepthwiseConv2d(num_filters * 8, kernel_size=3, stride=2, padding=1)
        self.bn12 = nn.BatchNorm2d(num_filters * 8)
        self.pointwise7 = nn.Conv2d(num_filters * 8, num_filters * 16, kernel_size=1, stride=1, padding=0, bias=False)
        self.bn13 = nn.BatchNorm2d(num_filters * 16)
        
        self.depthwise8 = DepthwiseConv2d(num_filters * 16, kernel_size=3, stride=1, padding=1)
        self.bn14 = nn.BatchNorm2d(num_filters * 16)
        self.pointwise8 = nn.Conv2d(num_filters * 16, num_filters * 16, kernel_size=1, stride=1, padding=0, bias=False)
        self.bn15 = nn.BatchNorm2d(num_filters * 16)

        self.depthwise9 = DepthwiseConv2d(num_filters * 16, kernel_size=3, stride=1, padding=1)
        self.bn16 = nn.BatchNorm2d(num_filters * 16)
        self.pointwise9 = nn.Conv2d(num_filters * 16, num_filters * 16, kernel_size=1, stride=1, padding=0, bias=False)
        self.bn17 = nn.BatchNorm2d(num_filters * 16)
        
        self.depthwise10 = DepthwiseConv2d(num_filters * 16, kernel_size=3, stride=1, padding=1)
        self.bn18 = nn.BatchNorm2d(num_filters * 16)
        self.pointwise10 = nn.Conv2d(num_filters * 16, num_filters * 16, kernel_size=1, stride=1, padding=0, bias=False)
        self.bn19 = nn.BatchNorm2d(num_filters * 16)
        
        self.depthwise11 = DepthwiseConv2d(num_filters * 16, kernel_size=3, stride=1, padding=1)
        self.bn20 = nn.BatchNorm2d(num_filters * 16)
        self.pointwise11 = nn.Conv2d(num_filters * 16, num_filters * 16, kernel_size=1, stride=1, padding=0, bias=False)
        self.bn21 = nn.BatchNorm2d(num_filters * 16)

        self.depthwise12 = DepthwiseConv2d(num_filters * 16, kernel_size=3, stride=1, padding=1)
        self.bn22 = nn.BatchNorm2d(num_filters * 16)
        self.pointwise12 = nn.Conv2d(num_filters * 16, num_filters * 16, kernel_size=1, stride=1, padding=0, bias=False)
        self.bn23 = nn.BatchNorm2d(num_filters * 16)

        self.depthwise13 = DepthwiseConv2d(num_filters * 16, kernel_size=3, stride=2, padding=1)
        self.bn24 = nn.BatchNorm2d(num_filters * 16)
        self.pointwise13 = nn.Conv2d(num_filters * 16, num_filters * 32, kernel_size=1, stride=1, padding=0, bias=False)
        self.bn25 = nn.BatchNorm2d(num_filters * 32)

        self.depthwise14 = DepthwiseConv2d(num_filters * 32, kernel_size=3, stride=1, padding=1)
        self.bn26 = nn.BatchNorm2d(num_filters * 32)
        self.pointwise14 = nn.Conv2d(num_filters * 32, num_filters * 32, kernel_size=1, stride=1, padding=0, bias=False)
        self.bn27 = nn.BatchNorm2d(num_filters * 32)
        
        self.avg_pool = nn.AdaptiveAvgPool2d((1, 1))
        self.fc = nn.Linear(num_filters * 32, num_classes)

    def forward(self, x):
        x = F.relu(self.bn1(self.conv1(x)))
        
        x = F.relu(self.bn2(self.depthwise2(x)))
        x = F.relu(self.bn3(self.pointwise2(x)))
        
        x = F.relu(self.bn4(self.depthwise3(x)))
        x = F.relu(self.bn5(self.pointwise3(x)))
        
        x = F.relu(self.bn6(self.depthwise4(x)))
        x = F.relu(self.bn7(self.pointwise4(x)))
        
        x = F.relu(self.bn8(self.depthwise5(x)))
        x = F.relu(self.bn9(self.pointwise5(x)))
        x = F.relu(self.bn10(self.depthwise6(x)))
        x = F.relu(self.bn11(self.pointwise6(x)))
        
        x = F.relu(self.bn12(self.depthwise7(x)))
        x = F.relu(self.bn13(self.pointwise7(x)))
        
        x = F.relu(self.bn14(self.depthwise8(x)))
        x = F.relu(self.bn15(self.pointwise8(x)))

        x = F.relu(self.bn16(self.depthwise9(x)))
        x = F.relu(self.bn17(self.pointwise9(x)))
        
        x = F.relu(self.bn18(self.depthwise10(x)))
        x = F.relu(self.bn19(self.pointwise10(x)))
        
        x = F.relu(self.bn20(self.depthwise11(x)))
        x = F.relu(self.bn21(self.pointwise11(x)))

        x = F.relu(self.bn22(self.depthwise12(x)))
        x = F.relu(self.bn23(self.pointwise12(x)))

        x = F.relu(self.bn24(self.depthwise13(x)))
        x = F.relu(self.bn25(self.pointwise13(x)))

        x = F.relu(self.bn26(self.depthwise14(x)))
        x = F.relu(self.bn27(self.pointwise14(x)))

        x = self.avg_pool(x)
        x = torch.flatten(x, 1)

        x = self.fc(x)
        
        return x

def get_model():
    
    model = MobileNetV1()

    return model
